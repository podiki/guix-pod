(define-module (darkman)
  #:use-module (gnu packages golang)
  #:use-module (gnu packages golang-build)
  #:use-module (gnu packages golang-check)
  #:use-module (gnu packages golang-xyz)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gnupg)
  #:use-module (gnu packages pkg-config)
  #:use-module (guix git-download)
  #:use-module (guix gexp)
  #:use-module (guix build-system go)
  #:use-module (gnu packages man)
  #:use-module (gnu packages base)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages))

(define-public go-github-com-adrg-xdg
  (package
    (name "go-github-com-adrg-xdg")
    (version "0.3.3")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/adrg/xdg")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32
            "1h11myyy426qrz96h9jrl7r1n8xwd5ybcr9c4s8l2pxn86dn5jsy"))))
    (build-system go-build-system)
    (arguments
     '(#:tests? #false ; fails trying to create directories
       #:import-path "github.com/adrg/xdg"))
    (propagated-inputs
      `(("go-github-com-stretchr-testify"
         ,go-github-com-stretchr-testify)))
    (home-page "https://github.com/adrg/xdg")
    (synopsis "Installation")
    (description
      "Package xdg provides an implementation of the XDG Base Directory
Specification.  The specification defines a set of standard paths for storing
application files including data and configuration files.  For portability and
flexibility reasons, applications should use the XDG defined locations instead
of hardcoding paths.  The package also includes the locations of well known user
directories.")
    (license license:expat)))

(define-public go-github-com-logrusorgru-aurora-v3
  (package
    (name "go-github-com-logrusorgru-aurora-v3")
    (version "3.0.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/logrusorgru/aurora")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0z7cgj8gl69271d0ag4f4yjbsvbrnfibc96cs01spqf5krv2rzjc"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/logrusorgru/aurora/v3"))
    (home-page "https://github.com/logrusorgru/aurora")
    (synopsis "Aurora")
    (description "Package aurora implements ANSI-colors")
    (license license:unlicense)))

(define-public go-github-com-sj14-astral
  (package
    (name "go-github-com-sj14-astral")
    (version "0.1.2")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/sj14/astral")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1rjr6d2rk2d7d8izd8v8rcx5vivfwqi5260y3g2spfq3f01jh8dg"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/sj14/astral"))
    (propagated-inputs
     (list go-gopkg-in-yaml-v3
           go-github-com-pmezard-go-difflib
           go-github-com-davecgh-go-spew
           go-github-com-stretchr-testify
           go-github-com-logrusorgru-aurora-v3))
    (home-page "https://github.com/sj14/astral")
    (synopsis "Astral")
    (description "Calculations for the position of the sun and moon.")
    (license license:asl2.0)))

(define-public go-github-com-russross-blackfriday-v2
  (package
    (name "go-github-com-russross-blackfriday-v2")
    (version "2.1.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/russross/blackfriday")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "0d1rg1drrfmabilqjjayklsz5d0n3hkf979sr3wsrw92bfbkivs7"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/russross/blackfriday/v2"))
    (home-page "https://github.com/russross/blackfriday")
    (synopsis "Blackfriday")
    (description "Package blackfriday is a markdown processor.")
    (license license:bsd-2)))

(define-public go-github-com-cpuguy83-go-md2man-v2
  (package
    (name "go-github-com-cpuguy83-go-md2man-v2")
    (version "2.0.1")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/cpuguy83/go-md2man")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "051ljpzf1f5nh631lvn53ziclkzmx5lza8545mkk6wxdfnfdcx8f"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/cpuguy83/go-md2man/v2"))
    (propagated-inputs
      `(("go-github-com-russross-blackfriday-v2"
         ,go-github-com-russross-blackfriday-v2)))
    (home-page "https://github.com/cpuguy83/go-md2man")
    (synopsis "go-md2man")
    (description "Converts markdown into roff (man pages).")
    (license license:expat)))

(define-public go-github-com-inconshreveable-mousetrap
  (package
    (name "go-github-com-inconshreveable-mousetrap")
    (version "1.0.0")
    (source
      (origin
        (method git-fetch)
        (uri (git-reference
               (url "https://github.com/inconshreveable/mousetrap")
               (commit (string-append "v" version))))
        (file-name (git-file-name name version))
        (sha256
          (base32 "1mn0kg48xkd74brf48qf5hzp0bc6g8cf5a77w895rl3qnlpfw152"))))
    (build-system go-build-system)
    (arguments '(#:import-path "github.com/inconshreveable/mousetrap"))
    (home-page "https://github.com/inconshreveable/mousetrap")
    (synopsis "mousetrap")
    (description "mousetrap is a tiny library that answers a single question.")
    (license license:asl2.0)))

(define-public go-github-com-integrii-flaggy
  (package
    (name "go-github-com-integrii-flaggy")
    (version "1.5.2")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/integrii/flaggy")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0qn55pn0c75bd4gm1fd2in0qp9fllfabwzn0qs994frd32cfz7h3"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/integrii/flaggy"))
    (propagated-inputs `(("go-github-com-google-go-cmp" ,go-github-com-google-go-cmp)))
    (home-page "https://github.com/integrii/flaggy")
    (synopsis "Installation")
    (description
     "Package flaggy is a input flag parsing package that supports recursive
subcommands, positional values, and any-position flags without unnecessary
complexeties.")
    (license license:unlicense)))

(define-public go-github-com-rxwycdh-rxhash
  (package
    (name "go-github-com-rxwycdh-rxhash")
    (version "0.0.0-20230131062142-10b7a38b400d")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/rxwycdh/rxhash")
             (commit (go-version->git-ref version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0qw4kn5r0xjfy9mycv57f7lmlpksybzr2qcdr4713svrxakwmgyz"))))
    (build-system go-build-system)
    (arguments
     (list
      #:import-path "github.com/rxwycdh/rxhash"))
    (home-page "https://github.com/rxwycdh/rxhash")
    (synopsis "rxhash")
    (description
     "rxhash is a Go library for creating a unique hash value for struct in Go, but
@@strong{data consistency}.")
    (license license:expat)))

(define-public darkman
  (package
    (name "darkman")
    (version "2.0.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://gitlab.com/WhyNotHugo/darkman.git")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32
         "0ylk2zgn1bf65214ph0qrk0zv5hm689x4d3c0qwscgpl5xbjk88m"))))
    (build-system go-build-system)
    (arguments
     (list #:import-path "gitlab.com/WhyNotHugo/darkman"
           ;; We don't need to install the source code for end-user applications.
           #:install-source? #f
           #:phases
           #~(modify-phases %standard-phases
               (add-after 'unpack 'patch-paths
                 (lambda _
                   (substitute* '("src/gitlab.com/WhyNotHugo/darkman/contrib/dbus/nl.whynothugo.darkman.service"
                                  "src/gitlab.com/WhyNotHugo/darkman/contrib/dbus/org.freedesktop.impl.portal.desktop.darkman.service"
                                  "src/gitlab.com/WhyNotHugo/darkman/darkman.service")
                                (("/usr/bin/darkman")
                                 (string-append #$output "/bin/darkman")))))
               (replace 'build
                 (lambda* (#:key import-path #:allow-other-keys)
                   (with-directory-excursion (string-append "src/" import-path)
                     (invoke "make" "build"))))
               (replace 'install
                 (lambda* (#:key import-path #:allow-other-keys)
                   (with-directory-excursion (string-append "src/" import-path)
                     (invoke "make" "install" (string-append "PREFIX=" #$output))))))))
    (inputs (list go-github-com-adrg-xdg
                  go-github-com-godbus-dbus-v5
                  go-github-com-rxwycdh-rxhash
                  go-github-com-spf13-cobra
                  go-github-com-sj14-astral
                  go-github-com-kr-pretty
                  go-gopkg-in-check-v1
                  go-gopkg-in-yaml-v3))
    (native-inputs (list scdoc))
    (home-page
     "https://gitlab.com/WhyNotHugo/darkman")
    (synopsis "darkman")
    (description
     "This package provides a framework for dark-mode and light-mode transitions on Linux
desktop.")
    (license license:isc)))

(define-public go-github-com-esiqveland-notify
  (package
    (name "go-github-com-esiqveland-notify")
    (version "0.13.3")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/esiqveland/notify")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "03d27a8rl55j6dqclg3233dj3j7v0i0p9cda06f9c4a21djamrfb"))))
    (build-system go-build-system)
    (arguments
     (list
      #:import-path "github.com/esiqveland/notify"))
    (propagated-inputs (list go-github-com-stretchr-testify
                             go-github-com-godbus-dbus-v5))
    (home-page "https://github.com/esiqveland/notify")
    (synopsis "notify")
    (description
     "Package notify is a wrapper around godbus for dbus notification interface See:
@@url{https://developer.gnome.org/notification-spec/,https://developer.gnome.org/notification-spec/}
and @@url{https://github.com/godbus/dbus,https://github.com/godbus/dbus}.")
    (license license:bsd-3)))

(define-public go-github-com-proglottis-gpgme
  (package
    (name "go-github-com-proglottis-gpgme")
    (version "0.1.4")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/proglottis/gpgme")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1hwa3hafay69m6mpg0jgwcbkw2fxligwp92pc3k3iwdn2zbfyzfm"))))
    (build-system go-build-system)
    (arguments
     (list
      #:tests? #f ;wants to run gpg-agent
      #:import-path "github.com/proglottis/gpgme"))
    (inputs (list gpgme))
    (native-inputs (list gnupg pkg-config))
    (home-page "https://github.com/proglottis/gpgme")
    (synopsis "GPGME (golang)")
    (description "Package gpgme provides a Go wrapper for the GPGME library.")
    (license license:bsd-3)))

(define-public go-github-com-rjeczalik-notify
  (package
    (name "go-github-com-rjeczalik-notify")
    (version "0.9.3")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/rjeczalik/notify")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1yl4yqx3qxi2adl6hpg4kzx6crhaksr237wnzhqmj89gqvvwgxmr"))))
    (build-system go-build-system)
    (arguments
     (list
      #:import-path "github.com/rjeczalik/notify"))
    (propagated-inputs (list go-golang-org-x-sys))
    (home-page "https://github.com/rjeczalik/notify")
    (synopsis "notify")
    (description "Package notify implements access to filesystem events.")
    (license license:expat)))

(define-public go-github-com-vtolstov-go-ioctl
  (package
    (name "go-github-com-vtolstov-go-ioctl")
    (version "0.0.0-20151206205506-6be9cced4810")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/vtolstov/go-ioctl")
             (commit (go-version->git-ref version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "11z60qlwci1vf08f5sibaw5hc0r3qh58cpq04886wr09kiipvnrv"))))
    (build-system go-build-system)
    (arguments
     (list
      #:import-path "github.com/vtolstov/go-ioctl"))
    (home-page "https://github.com/vtolstov/go-ioctl")
    (synopsis "go-ioctl")
    (description
     "Documentation @@url{http://godoc.org/github.com/vtolstov/go-ioctl,(img (@@ (src
https://godoc.org/github.com/vtolstov/go-ioctl?status.svg) (alt
@code{GoDoc})))}.")
    (license license:expat)))

(define-public yubikey-touch-detector
  (package
    (name "yubikey-touch-detector")
    (version "0.0.0-20250110002301-32216e4348d2")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/maximbaz/yubikey-touch-detector")
             (commit (go-version->git-ref version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "0l4l6hz6dirndc3f7md6828yx9qf022pnw7nwbzg66xa0wxkkkpk"))))
    (build-system go-build-system)
    (arguments
     (list
      #:import-path "github.com/maximbaz/yubikey-touch-detector"))
    (inputs (list gpgme))
    (native-inputs (list pkg-config))
    (propagated-inputs (list go-github-com-deckarep-golang-set
                             go-github-com-vtolstov-go-ioctl
                             go-github-com-sirupsen-logrus
                             go-github-com-rjeczalik-notify
                             go-github-com-proglottis-gpgme
                             go-github-com-godbus-dbus-v5
                             go-github-com-esiqveland-notify
                             go-github-com-coreos-go-systemd-v22))
    (home-page "https://github.com/maximbaz/yubikey-touch-detector")
    (synopsis "YubiKey touch detector")
    (description
     "This is a tool that can detect when @code{YubiKey} is waiting for your touch.
It is designed to be integrated with other UI components to display a visible
indicator.")
    (license license:isc)))

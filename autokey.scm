(define-module (autokey)
  #:use-module (guix packages)
  ;; for canvasapi
  #:use-module (guix download)
  #:use-module (gnu packages time)
  #:use-module (gnu packages python-web)
  ;; flashfocus
  #:use-module (gnu packages libffi)
  ;; o365
  #:use-module (guix build-system pyproject)
  #:use-module (guix gexp)
  #:use-module (gnu packages python-check)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages sphinx)
  ;;
  #:use-module (guix git-download)
  #:use-module (guix build-system python)
  #:use-module (gnu packages)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages check)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages qt)
  ;; legendary
  #:use-module (gnu packages python-build)
  ;; lutris
  #:use-module (srfi srfi-1) ; append-map, delete-duplicates
  #:use-module (guix utils)
  #:use-module (gnu packages python-crypto)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages webkit)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages audio)
  #:use-module (gnu packages xml)
  #:use-module ((guix licenses) #:prefix license:))

(define-public python-stringcase
  (package
    (name "python-stringcase")
    (version "1.2.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "stringcase" version))
              (sha256
               (base32
                "023hv3gknblhf9lx5kmkcchzmbhkdhmsnknkv7lfy20rcs06k828"))))
    (build-system python-build-system)
    (home-page "https://github.com/okunishinishi/python-stringcase")
    (synopsis "String case converter.")
    (description "String case converter.")
    (license license:expat)))

(define-public python-sphinx-argparse-cli
  (package
    (name "python-sphinx-argparse-cli")
    (version "1.11.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "sphinx_argparse_cli" version))
              (sha256
               (base32
                "1hif4jhvckzzpvk1bxd6l0z77s4b9g3q6idf8nm2jsj5m0p03r83"))))
    (build-system pyproject-build-system)
    (propagated-inputs (list python-sphinx))
    (native-inputs (list python-covdefaults python-hatchling python-hatch-vcs python-pytest python-pytest-cov))
    (home-page "")
    (synopsis
     "render CLI arguments (sub-commands friendly) defined by argparse module")
    (description
     "render CLI arguments (sub-commands friendly) defined by argparse module")
    (license license:expat)))

(define-public python-sphinx-basic-ng
  (package
    (name "python-sphinx-basic-ng")
    (version "1.0.0b1")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "sphinx_basic_ng" version))
              (sha256
               (base32
                "1c59nmxzqxga8lc42krd1yb9k3kir0l1wy462wq2lifrrk9lndw9"))))
    (build-system python-build-system)
    (propagated-inputs (list python-sphinx))
    (home-page "https://github.com/pradyunsg/sphinx-basic-ng")
    (synopsis "A modern skeleton for Sphinx themes.")
    (description "This package provides a modern skeleton for Sphinx themes.")
    (license license:expat)))

;; tries to use node?
(define-public python-furo
  (package
    (name "python-furo")
    (version "2023.3.27")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "furo" version))
              (sha256
               (base32
                "19f56fxk95khnpdiy1wkkyicfn35vlqhc8yp6hmkp0yclmkpi7mr"))))
    (build-system pyproject-build-system)
    (propagated-inputs (list python-beautifulsoup4 python-pygments
                             python-sphinx python-sphinx-basic-ng
                             python-sphinx-theme-builder))
    (home-page "")
    (synopsis "A clean customisable Sphinx documentation theme.")
    (description
     "This package provides a clean customisable Sphinx documentation theme.")
    (license license:expat)))

(define-public python-pyproject-hooks
  (package
    (name "python-pyproject-hooks")
    (version "1.0.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "pyproject_hooks" version))
              (sha256
               (base32
                "1xaf4sharvacqlav6w3b38nc4j0rzg0p4axi7zamanbzp6cb4wgj"))))
    (build-system pyproject-build-system)
    (propagated-inputs (list python-tomli python-flit-core))
    (native-inputs (list python-pytest python-testpath))
    (home-page "")
    (synopsis "Wrappers to call pyproject.toml-based build backend hooks.")
    (description "Wrappers to call pyproject.toml-based build backend hooks.")
    (license license:expat)))

(define-public python-build
  (package
    (name "python-build")
    (version "0.10.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "build" version))
              (sha256
               (base32
                "0scj5k586n8rmnlqhay5j43ci1z8ip3sm0j4f3b52nfvmxj15dym"))))
    (build-system python-build-system)
    (propagated-inputs (list python-colorama
                             python-filelock
                             ;python-furo
                             python-importlib-metadata
                             python-mypy
                             python-packaging
                             python-pyproject-hooks
                             python-pytest
                             python-pytest-cov
                             python-pytest-mock
                             python-pytest-rerunfailures
                             python-pytest-xdist
                             python-setuptools
                             python-sphinx
                             python-sphinx-argparse-cli
                             python-sphinx-autodoc-typehints
                             python-toml
                             python-tomli
                             python-typing-extensions
                             python-virtualenv
                             python-wheel))
    (home-page "")
    (synopsis "A simple, correct Python build frontend")
    (description
     "This package provides a simple, correct Python build frontend")
    (license license:expat)))

(define-public python-zest.releaser
  (package
    (name "python-zest.releaser")
    (version "7.3.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "zest.releaser" version))
              (sha256
               (base32
                "049d551w3ymr1p6z0jf3xys6bqbqvdbq2ymwbc17yr91kib1xskn"))))
    (build-system python-build-system)
    (propagated-inputs (list python-colorama python-requests python-setuptools
                             python-twine))
    (native-inputs (list python-wheel python-zope-testing
                         python-zope-testrunner))
    (home-page "https://zestreleaser.readthedocs.io")
    (synopsis "Software releasing made easy and repeatable")
    (description "Software releasing made easy and repeatable")
    (license license:gpl3)))

(define-public python-pyroma
  (package
    (name "python-pyroma")
    (version "4.2")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "pyroma" version))
              (sha256
               (base32
                "0g1qwzqwmkz3k3nfynwr444cm6flxd3zpmdf9wki43m1lz27swkc"))))
    (build-system python-build-system)
    (propagated-inputs (list python-build
                             python-docutils
                             python-packaging
                             python-pygments
                             python-requests
                             python-setuptools
                             ;python-trove-classifiers
                             ))
    (native-inputs (list python-setuptools python-zest.releaser))
    (home-page "https://github.com/regebro/pyroma")
    (synopsis "Test your project's packaging friendliness")
    (description "Test your project's packaging friendliness")
    (license license:expat)))

(define-public python-pytz-deprecation-shim
  (package
    (name "python-pytz-deprecation-shim")
    (version "0.1.0.post0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "pytz_deprecation_shim" version))
              (sha256
               (base32
                "17d58msbi18dc4lk29hcrgylvrv9vhniwi24axfdwvb13fp7n2dg"))))
    (build-system pyproject-build-system)
    (arguments `(#:tests? #f))
    (propagated-inputs (list ;python-backports-zoneinfo
                             python-dateutil
                             python-tzdata))
    (home-page "https://github.com/pganssle/pytz-deprecation-shim")
    (synopsis "Shims to make deprecation of pytz easier")
    (description "Shims to make deprecation of pytz easier")
    (license license:expat)))

(define-public python-o365
  (package
    (name "python-o365")
    (version "2.0.26")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "O365" version))
              (sha256
                (base32
                  "0f6yfs5w096mzsa6cyr5pwa6ni5arkpsnsj4k3115w9dclm54y5l"))))
    (build-system pyproject-build-system)
    (arguments `(#:tests? #f))
    (native-inputs (list python-setuptools python-wheel))
    (propagated-inputs (list python-beautifulsoup4
                             python-dateutil
                             python-pytz
                             python-requests
                             python-requests-oauthlib
                             python-stringcase
                             python-tzlocal))
    (home-page "https://github.com/O365/python-o365")
    (synopsis "Microsoft Graph and Office 365 API made easy")
    (description "Microsoft Graph and Office 365 API made easy")
    (license license:asl2.0)))

(define-public python-google-auth-oauthlib
  (package
    (name "python-google-auth-oauthlib")
    (version "1.0.0")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "google-auth-oauthlib" version))
              (sha256
               (base32
                "1ifggsv8mxw5jzhy7yf34diin1bpzphpw6vy38i4f2w2ci4hcxg3"))))
    (build-system python-build-system)
    (arguments `(#:tests? #f)) ; network
    (propagated-inputs (list python-click python-google-auth python-requests-oauthlib))
    (home-page
     "https://github.com/GoogleCloudPlatform/google-auth-library-python-oauthlib")
    (synopsis "Google Authentication Library")
    (description "Google Authentication Library")
    (license license:asl2.0)))

(define python-xlib-patched
  (package
    (inherit python-xlib)
    (version "0.31")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/python-xlib/python-xlib")
             (commit version)))
       (file-name (git-file-name "python-xlib" version))
       (sha256
        (base32 "155p9xhsk01z9vdml74h07svlqy6gljnx9c6qbydcr14lwghwn06"))
       (patches (search-patches "python-xlib-no-display.patch"))))))

(define-public python-legendary-gl
  (package
    (name "python-legendary-gl")
    (version "0.20.9")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "legendary-gl" version))
        (sha256
          (base32
            "10s4f2vmc5a0yic93jkkb8s7l8zaf94dswz29g14i3bf3rq0dk0y"))))
    (build-system python-build-system)
    (propagated-inputs
      `(("python-requests" ,python-requests)
        ("python-setuptools" ,python-setuptools)
        ("python-wheel" ,python-wheel)))
    (arguments
     `(#:tests? #f))
    (home-page "https://github.com/derrod/legendary")
    (synopsis
      "Free and open-source replacement for the Epic Games Launcher application")
    (description
      "Free and open-source replacement for the Epic Games Launcher application")
    (license #f)))

(define-public python-lutris
  (package
    (name "python-lutris")
    (version "0.5.8.4")
    (source (origin
             (method git-fetch)
             (uri (git-reference
                   (url "https://github.com/lutris/lutris")
                   (commit (string-append "v" version))))
             (file-name (git-file-name name version))
             (sha256
              (base32
               "0lsblbkchdj2wrxflbqw12zc69ffw19kq8g5pv8x6czj00idfaz6"))))
    (build-system python-build-system)
    (inputs
      `(
    ;;     ("libappindicator" ,libappindicator)
    ;;     ("libnotify" ,libnotify)
    ;;     ;; required for future version
    ;;     ; ("wmctrl" ,wmctrl)
    ;;     ; ("zenity" ,zenity)
    ;;     ("gdk-pixbuf" ,gdk-pixbuf+svg)
    ;;     ("glib" ,glib)
        ("gtk+" ,gtk+)
    ;;     ("gtksourceview" ,gtksourceview-3)
        ("gobject-introspection" ,gobject-introspection)
        ))
    (propagated-inputs
     `(("python-magic" ,python-magic)
       ("python-requests" ,python-requests)
       ("python-evdev" ,python-evdev)
       ;; ("python-dbus" ,python-dbus)
       ("python-pygobject" ,python-pygobject)
       ("python-pyyaml" ,python-pyyaml)
        ;; ("python-pyinotify" ,python-pyinotify)
        ;; ("python-xlib-patched" ,python-xlib-patched)
        ;; ("python-pycairo" ,python-pycairo)
        ;; ("python-packaging" ,python-packaging)
        ;; ("python-pyinotify" ,python-pyinotify
       ))
    (arguments
     `(#:tests? #f))
    (home-page "https://github.com/autokey/autokey")
    (synopsis
      "Keyboard and GUI automation on Linux (X11)")
    (description
      "Keyboard and GUI automation on Linux (X11)")
    (license license:gpl3)))

(define-public python-i3ipc
  (package
    (name "python-i3ipc")
    (version "2.2.1")
    (source (origin
              (method url-fetch)
              (uri (pypi-uri "i3ipc" version))
              (sha256
               (base32
                "1s6crkdn7q8wmzl5d0pb6rdkhhbvp444yxilrgaylnbr2kbxg078"))))
    (build-system python-build-system)
    (arguments
     `(#:tests? #f)) ;no tests?
    (propagated-inputs (list python-xlib))
    (home-page "https://github.com/altdesktop/i3ipc-python")
    (synopsis "An improved Python library to control i3wm and sway")
    (description "An improved Python library to control i3wm and sway")
    (license license:bsd-3)))

(define-public python-xpybutil
  (package
    (name "python-xpybutil")
    (version "0.0.6")
    (source (origin
             (method git-fetch)
             (uri (git-reference
                   (url "https://github.com/BurntSushi/xpybutil")
                   (commit version)))
             (file-name (git-file-name "python-xpybutil" version))
             (sha256
              (base32 "17gbqq955fcl29aayn8l0x14azc60cxgkvdxblz9q8x3l50w0xpg"))))
    (build-system python-build-system)
    (propagated-inputs (list python-pillow python-xcffib))
    (home-page "https://github.com/altdesktop/i3ipc-python")
    (synopsis "An improved Python library to control i3wm and sway")
    (description "An improved Python library to control i3wm and sway")
    (license license:bsd-3)))

(define-public flashfocus
  (package
  (name "flashfocus")
  (version "2.2.5")
  (source (origin
            (method url-fetch)
            (uri (pypi-uri "flashfocus" version))
            (sha256
             (base32
              "01527m0rdjqn968pqi8radi554bbgiad1dz6mq5mni3ycnkk8amz"))))
  (build-system python-build-system)
  (arguments
   `(#:tests? #f)) ; no tests?
  (propagated-inputs (list procps
                           python-cffi
                           python-click
                           python-i3ipc
                           python-marshmallow
                           python-pyyaml
                           python-xcffib
                           python-xpybutil))
  (home-page "https://www.github.com/fennerm/flashfocus")
  (synopsis "Simple focus animations for tiling window managers")
  (description "Simple focus animations for tiling window managers")
  (license license:expat)))

(define-module (openrgb)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python)
  #:use-module (gnu packages libusb)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages)
  #:use-module (guix git-download)
  #:use-module (guix build-system cmake)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages cpp)
  #:use-module (gnu packages hardware)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages audio)
  ;; core ctrl
  #:use-module (guix utils)
  #:use-module (guix gexp)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages polkit)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages kde-frameworks)
  #:use-module (guix build-system qt)
  #:use-module (gnu packages crypto)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages check)
  #:use-module (gnu packages logging)
  #:use-module (gnu packages pciutils)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages vulkan)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages))

(define-public openrgbeffectsplugin
  (package
    (name "openrgbeffectsplugin")
    (version "0.8")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://gitlab.com/OpenRGBDevelopers/OpenRGBEffectsPlugin")
             (commit (string-append "release_" version))
             (recursive? #t)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1s5025bxddkjg9fh8qraigk3inamfpkygyw8q014qix0nmwb4pnq"))
       (modules '((guix build utils)))
       (snippet
        '(begin
           ;; Delete many of the bundled libraries.
           (delete-file-recursively "OpenRGB")))))
    (build-system cmake-build-system)
    (arguments
     (list
       #:tests? #f ; doesn't have tests
       #:phases
       #~(modify-phases %standard-phases
           (add-after 'unpack 'openrgb-sources
             (lambda _
               (copy-recursively #$(package-source (this-package-input "openrgb")) "OpenRGB")))
           (add-after 'openrgb-sources 'unbundle
             (lambda* (#:key inputs #:allow-other-keys)
               (substitute* "OpenRGBEffectsPlugin.pro"
                 (("OpenRGB/dependencies/json")
                  (string-append #$(this-package-input "json-modern-cxx")
                                 "/include/nlohmann")))))
           ;; Call qmake instead of configure to create a Makefile.
           (replace 'configure
             (lambda _
               (invoke "qmake" ".")))
           (replace 'install
             (lambda _
               (for-each (lambda (file)
                           (install-file file (string-append #$output "/lib/openrgb/plugins")))
                         (find-files "." "libOpenRGB*")))))))
    (inputs (list json-modern-cxx openal openrgb))
    (native-inputs (list qtbase-5)) ; for qmake
    (synopsis "OpenRGB effects plugin")
    (description
     "Effects")
    (home-page "https://openrgb.org/")
    (license license:gpl2)))

(define-public openrgbskinplugin
  (package
    (name "openrgbskinplugin")
    (version "0.8")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://gitlab.com/OpenRGBDevelopers/OpenRGBSkinPlugin")
             (commit (string-append "release_" version))
             (recursive? #t)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1xxylanway8bj9nvqkkwivwlfc2h5fkgkxkfskhb96ssmjazzqgn"))))
    (build-system cmake-build-system)
    (arguments
     (list
       #:tests? #f ; doesn't have tests
       #:phases
       #~(modify-phases %standard-phases
           (add-after 'unpack 'openrgb-sources
             (lambda _
               (copy-recursively #$(package-source (this-package-input "openrgb")) "OpenRGB")))
           (add-after 'openrgb-sources 'unbundle
             (lambda* (#:key inputs #:allow-other-keys)
               (substitute* "OpenRGBSkinPlugin.pro"
                 (("OpenRGB/dependencies/json")
                  (string-append #$(this-package-input "json-modern-cxx")
                                 "/include/nlohmann")))))
           ;; Call qmake instead of configure to create a Makefile.
           (replace 'configure
             (lambda _
               (invoke "qmake" ".")))
           (replace 'install
             (lambda _
               (for-each (lambda (file)
                           (install-file file (string-append #$output "/lib/openrgb/plugins")))
                         (find-files "." "libOpenRGB*")))))))
    (inputs (list json-modern-cxx openrgb))
    (native-inputs (list qtbase-5)) ; for qmake
    (synopsis "OpenRGB effects plugin")
    (description
     "Effects")
    (home-page "https://openrgb.org/")
    (license license:gpl2)))

(define-public openrgbvisualmapplugin
  (package
    (name "openrgbvisualmapplugin")
    (version "0.8")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://gitlab.com/OpenRGBDevelopers/OpenRGBVisualMapPlugin")
             (commit (string-append "release_" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "07cqmlbd7ryyclhdzfs4mfangwr3cp8rh456wslkv9dw8mqq4afh"))))
    (build-system cmake-build-system)
    (arguments
     (list
       #:tests? #f ; doesn't have tests
       #:phases
       #~(modify-phases %standard-phases
           (add-after 'unpack 'openrgb-sources
             (lambda _
               (copy-recursively #$(package-source (this-package-input "openrgb")) "OpenRGB")))
           (add-after 'openrgb-sources 'unbundle
             (lambda* (#:key inputs #:allow-other-keys)
               (substitute* "OpenRGBVisualMapPlugin.pro"
                 (("OpenRGB/dependencies/json")
                  (string-append #$(this-package-input "json-modern-cxx")
                                 "/include/nlohmann")))))
           ;; Call qmake instead of configure to create a Makefile.
           (replace 'configure
             (lambda _
               (invoke "qmake" ".")))
           (replace 'install
             (lambda _
               (for-each (lambda (file)
                           (install-file file (string-append #$output "/lib/openrgb/plugins")))
                         (find-files "." "libOpenRGB*")))))))
    (inputs (list json-modern-cxx openrgb))
    (native-inputs (list qtbase-5)) ; for qmake
    (synopsis "OpenRGB effects plugin")
    (description
     "Effects")
    (home-page "https://openrgb.org/")
    (license license:gpl2)))

(define-public openrgbschedulerplugin
  (package
    (name "openrgbschedulerplugin")
    (version "0.8")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://gitlab.com/OpenRGBDevelopers/OpenRGBSchedulerPlugin")
             (commit (string-append "release_" version))
             (recursive? #t)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "06zd1slxxhw9jmxag6c9y7s4a1l8wryhz6hhmvnc8rrq8vs3qrv9"))))
    (build-system cmake-build-system)
    (arguments
     (list
       #:tests? #f ; doesn't have tests
       #:phases
       #~(modify-phases %standard-phases
           ;; Call qmake instead of configure to create a Makefile.
           (replace 'configure
             (lambda _
               (invoke "qmake" "OpenRGBSchedulerPlugin.pro")))
           (replace 'install
             (lambda _
               (for-each (lambda (file)
                           (install-file file (string-append #$output "/lib/openrgb/plugins")))
                         (find-files "." "libOpenRGBSchedulerPlugin.*")))))))
    (native-inputs (list qtbase-5)) ; for qmake
    (synopsis "OpenRGB effects plugin")
    (description
     "Effects")
    (home-page "https://openrgb.org/")
    (license license:gpl2)))

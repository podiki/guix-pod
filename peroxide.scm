;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2022 John Kehayias <john.kehayias@protonmail.com>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.


(define-module (peroxide)
  #:use-module (gnu)
  #:use-module (gnu packages base)
  #:use-module (gnu packages golang)
  #:use-module (gnu packages golang-build)
  #:use-module (gnu packages golang-check)
  #:use-module (gnu packages golang-crypto)
  #:use-module (gnu packages golang-web)
  #:use-module (gnu packages golang-xyz)
  #:use-module (gnu packages databases)
  #:use-module (gnu packages syncthing)
  #:use-module (darkman)
  #:use-module (guix build-system go)
  #:use-module (guix download)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:))

(define-public go-github-com-antlr-antlr4-runtime-go-antlr
  (package
    (name "go-github-com-antlr-antlr4-runtime-go-antlr")
    (version "0.0.0-20220826213629-cd8f367ca010")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/antlr/antlr4")
                    (commit (go-version->git-ref version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "10p3z59xki0lw6wwbrsan3vrv636q049p56gligwr2kd71mh0agb"))))
    (build-system go-build-system)
    (arguments
     '(#:tests? #f ; failed
       #:import-path "github.com/antlr/antlr4/runtime/Go/antlr"
       #:unpack-path "github.com/antlr/antlr4"))
    (home-page "https://github.com/antlr/antlr4")
    (synopsis #f)
    (description
     "Copyright (c) 2012-2017 The ANTLR Project.  All rights reserved.  Use of this
file is governed by the BSD 3-clause license that can be found in the
LICENSE.txt file in the project root.")
    (license license:bsd-3)))

(define-public go-github-com-protonmail-go-rfc5322
  (package
    (name "go-github-com-protonmail-go-rfc5322")
    (version "0.11.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/ProtonMail/go-rfc5322")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0sx15vcwpc0w894c127jbq0daxh0chrlcp6shhc0a3qz1zi6c1rd"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/ProtonMail/go-rfc5322"))
    (propagated-inputs `(("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
                         ("go-github-com-antlr-antlr4-runtime-go-antlr" ,go-github-com-antlr-antlr4-runtime-go-antlr)))
    (home-page "https://github.com/ProtonMail/go-rfc5322")
    (synopsis "Outline")
    (description
     "The @code{rfc5322} package implements a parser for @code{address-list} and
@code{date-time} strings, as defined in RFC5322.  It also supports encoded words
(RFC2047) and has international tokens (RFC6532).")
    (license license:expat)))

(define-public go-github-com-cronokirby-saferith
  (package
    (name "go-github-com-cronokirby-saferith")
    (version "0.33.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/cronokirby/saferith")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0g510g9zcqp6rjjxlqdv72jsb0m7hmahcia3nvmw5gac1nml7q2q"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/cronokirby/saferith"))
    (home-page "https://github.com/cronokirby/saferith")
    (synopsis "saferith")
    (description
     "The purpose of this package is to provide a version of arbitrary sized
arithmetic, in a safer (i.e.  constant-time) way, for cryptography.")
    (license license:expat)))

(define-public go-github-com-protonmail-go-srp
  (package
    (name "go-github-com-protonmail-go-srp")
    (version "0.0.7")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/ProtonMail/go-srp")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0bzpq1yqfrrzyrmmwc9kf84k35567hdrs4zagxakpi7hia847l1z"))))
    (build-system go-build-system)
    (arguments
     '(#:tests? #f ; started to fail, not sure which tests or why
       #:import-path "github.com/ProtonMail/go-srp"))
    (propagated-inputs `(;("go-github-com-protonmail-go-mobile" ,go-github-com-protonmail-go-mobile)
                         ("go-golang-org-x-crypto" ,go-golang-org-x-crypto)
                         ("go-github-com-pkg-errors" ,go-github-com-pkg-errors)
                         ("go-github-com-cronokirby-saferith" ,go-github-com-cronokirby-saferith)
                         ("go-github-com-protonmail-go-crypto" ,go-github-com-protonmail-go-crypto)
                         ("go-github-com-protonmail-bcrypt" ,go-github-com-protonmail-bcrypt)))
    (home-page "https://github.com/ProtonMail/go-srp")
    (synopsis "go-srp")
    (description
     "Golang implementation of the
@url{https://datatracker.ietf.org/doc/html/rfc5054,SRP protocol}, used for
authentication of ProtonMail users.")
    (license license:expat)))

(define-public go-github-com-protonmail-go-vcard
  (package
    (name "go-github-com-protonmail-go-vcard")
    (version "0.0.0-20180326232728-33aaa0a0c8a5")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/ProtonMail/go-vcard")
                    (commit (go-version->git-ref version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "19cify6lxd2yirqc92yfgzvn5qlc8a01a2kxjdg83jv0lx6ps26q"))))
    (build-system go-build-system)
    (arguments
     '(#:tests? #f ; fails?
       #:import-path "github.com/ProtonMail/go-vcard"))
    (propagated-inputs (list go-github-com-emersion-go-vcard))
    (home-page "https://github.com/ProtonMail/go-vcard")
    (synopsis "go-vcard")
    (description "Package vcard implements the vCard format, defined in
@url{https://rfc-editor.org/rfc/rfc6350.html,RFC 6350}.")
    (license license:expat)))

(define-public go-github-com-protonmail-go-crypto
  (package
    (name "go-github-com-protonmail-go-crypto")
    (version "0.0.0-20220824120805-4b6e5c587895")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/ProtonMail/go-crypto")
                    (commit (go-version->git-ref version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1ih16krilgd9nsnvzyfszc1wfzcprm0rd0bgl7ccnhlz6vhmcz1x"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/ProtonMail/go-crypto"
       #:tests? #f ; source only
       #:phases (modify-phases %standard-phases
                  ;; source only package
                  (delete 'build))))
    (propagated-inputs `(("go-golang-org-x-crypto" ,go-golang-org-x-crypto)
                         ("go-github-com-cloudflare-circl" ,go-github-com-cloudflare-circl)))
    (home-page "https://github.com/ProtonMail/go-crypto")
    (synopsis #f)
    (description
     "This module is backwards compatible with x/crypto/openpgp, so you can simply
replace all imports of @code{golang.org/x/crypto/openpgp} with
@code{github.com/ProtonMail/go-crypto/openpgp}.")
    (license license:bsd-3)))

(define-public go-github-com-protonmail-go-mime
  (package
    (name "go-github-com-protonmail-go-mime")
    (version "0.0.0-20220429130430-2192574d760f")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/ProtonMail/go-mime")
                    (commit (go-version->git-ref version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1c2h6kn6i2719qdci8cvad77dl0hmcfjpb0ax9prczwcypcnbx89"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/ProtonMail/go-mime"))
    (propagated-inputs `(("go-golang-org-x-text" ,go-golang-org-x-text)))
    (home-page "https://github.com/ProtonMail/go-mime")
    (synopsis "Go Mime Wrapper Library")
    (description "This package provides a parser for MIME messages")
    (license license:expat)))

(define-public go-github-com-protonmail-gopenpgp-v2
  (package
    (name "go-github-com-protonmail-gopenpgp-v2")
    (version "2.4.10")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/ProtonMail/gopenpgp")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "14jrpd2ggafhxqdccb516j9v4v7760sck9kgz1m274a2qrs07xqb"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/ProtonMail/gopenpgp/v2"
       #:tests? #f
       #:phases
       (modify-phases %standard-phases
         (delete 'build))))
    (propagated-inputs `(;("go-github-com-protonmail-go-mobile" ,go-github-com-protonmail-go-mobile)
                         ("go-golang-org-x-crypto" ,go-golang-org-x-crypto)
                         ("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
                         ("go-github-com-pkg-errors" ,go-github-com-pkg-errors)
                         ("go-github-com-protonmail-go-mime" ,go-github-com-protonmail-go-mime)
                         ("go-github-com-protonmail-go-crypto" ,go-github-com-protonmail-go-crypto)))
    (home-page "https://github.com/ProtonMail/gopenpgp")
    (synopsis "GopenPGP V2")
    (description "GopenPGP is a high-level OpenPGP library built on top of .")
    (license license:expat)))

(define-public go-github-com-emersion-go-imap-appendlimit
  (package
    (name "go-github-com-emersion-go-imap-appendlimit")
    (version "0.0.0-20210907172056-e3baed77bbe4")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/emersion/go-imap-appendlimit")
                    (commit (go-version->git-ref version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0fb2v51k0r5aipz8r2sz6yw0x7kia6yx40ly9v0942ifc44dchhh"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/emersion/go-imap-appendlimit"))
    (propagated-inputs (list go-github-com-emersion-go-imap go-github-com-emersion-go-sasl go-golang-org-x-text))
    (home-page "https://github.com/emersion/go-imap-appendlimit")
    (synopsis "go-imap-appendlimit")
    (description
     "This package implements the IMAP APPENDLIMIT Extension, as defined in
@url{https://rfc-editor.org/rfc/rfc7889.html,RFC 7889}.")
    (license license:expat)))

(define-public go-github-com-emersion-go-imap-move
  (package
    (name "go-github-com-emersion-go-imap-move")
    (version "0.0.0-20210907172020-fe4558f9c872")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/emersion/go-imap-move")
                    (commit (go-version->git-ref version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1aw98d6hlnmbzn1z83v4lqfgr7wm09cp5gfx5ibwzhqi0whdyr18"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/emersion/go-imap-move"))
    (propagated-inputs (list go-github-com-emersion-go-imap go-github-com-emersion-go-sasl go-golang-org-x-text))
    (home-page "https://github.com/emersion/go-imap-move")
    (synopsis "go-imap-move")
    (description "This package implements the IMAP MOVE Extension, defined in
@url{https://rfc-editor.org/rfc/rfc6851.html,RFC 6851}.")
    (license license:expat)))

(define-public go-github-com-emersion-go-imap-quota
  (package
    (name "go-github-com-emersion-go-imap-quota")
    (version "0.0.0-20210203125329-619074823f3c")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/emersion/go-imap-quota")
                    (commit (go-version->git-ref version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0qm0gbl43b1l017j2f3in55mk2k69fixjkwdbzv9vcvmikqbcib8"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/emersion/go-imap-quota"))
    (propagated-inputs (list go-github-com-emersion-go-imap go-github-com-emersion-go-sasl go-golang-org-x-text))
    (home-page "https://github.com/emersion/go-imap-quota")
    (synopsis "go-imap-quota")
    (description
     "This package implements the IMAP QUOTA extension, as defined in
@url{https://rfc-editor.org/rfc/rfc2087.html,RFC 2087}.")
    (license license:expat)))

(define-public go-github-com-emersion-go-imap-unselect
  (package
    (name "go-github-com-emersion-go-imap-unselect")
    (version "0.0.0-20210907172115-4c2c4843bf69")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/emersion/go-imap-unselect")
                    (commit (go-version->git-ref version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "16246gyshbwh4dz3ds2hqhcr15alnjdgyv3kmd7016shxpajkyx3"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/emersion/go-imap-unselect"))
    (propagated-inputs (list go-github-com-emersion-go-imap go-github-com-emersion-go-sasl go-golang-org-x-text))
    (home-page "https://github.com/emersion/go-imap-unselect")
    (synopsis "go-imap-unselect")
    (description
     "This package implements the IMAP UNSELECT Extension, defined in
@url{https://rfc-editor.org/rfc/rfc3691.html,RFC 3691}.")
    (license license:expat)))

(define-public go-github-com-emersion-go-smtp
  (package
    (name "go-github-com-emersion-go-smtp")
    (version "0.15.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/emersion/go-smtp")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1vhc0vpjd4yhxk6wrh01sdpi7nprjn98s46yy82xwlkm0cskl0h7"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/emersion/go-smtp"))
    (propagated-inputs `(("go-github-com-emersion-go-sasl" ,go-github-com-emersion-go-sasl)))
    (home-page "https://github.com/emersion/go-smtp")
    (synopsis "go-smtp")
    (description
     "Package smtp implements the Simple Mail Transfer Protocol as defined in
@url{https://rfc-editor.org/rfc/rfc5321.html,RFC 5321}.")
    (license license:expat)))

(define-public go-github-com-emersion-go-vcard
  (package
    (name "go-github-com-emersion-go-vcard")
    (version "0.0.0-20220507122617-d4056df0ec4a")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/emersion/go-vcard")
                    (commit (go-version->git-ref version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "04yjzg0fgc6ldf603mkh99k1p1fp30ni0j1141abi1jv04sdk919"))))
    (build-system go-build-system)
    (arguments
     '(#:tests? #f ; fails?
       #:import-path "github.com/emersion/go-vcard"))
    (home-page "https://github.com/emersion/go-vcard")
    (synopsis "go-vcard")
    (description "Package vcard implements the vCard format, defined in
@url{https://rfc-editor.org/rfc/rfc6350.html,RFC 6350}.")
    (license license:expat)))

(define-public go-github-com-ghodss-yaml
  (package
    (name "go-github-com-ghodss-yaml")
    (version "1.0.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/ghodss/yaml")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0skwmimpy7hlh7pva2slpcplnm912rp3igs98xnqmn859kwa5v8g"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/ghodss/yaml"))
    (propagated-inputs (list go-gopkg-in-yaml-v2))
    (home-page "https://github.com/ghodss/yaml")
    (synopsis "YAML marshaling and unmarshaling support for Go")
    (description
     "Copyright 2013 The Go Authors.  All rights reserved.  Use of this source code is
governed by a BSD-style license that can be found in the LICENSE file.")
    (license #f)))

(define-public go-golang-org-x-net-old
  (let ((commit "ba9fcec4b297b415637633c5a6e8fa592e4a16c3")
        (revision "4"))
    (package
      (name "go-golang-org-x-net-old")
      (version (git-version "0.0.0" revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://go.googlesource.com/net")
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32
                  "1hbqvy6r0s5h0dpdqw8fynl3cq0acin3iyqki9xvl5r8h33yb9bx"))))
      (build-system go-build-system)
      (arguments
       `(#:import-path "golang.org/x/net"
         ; Source-only package
         #:tests? #f
         #:phases
         (modify-phases %standard-phases
           (delete 'build))))
      (synopsis "Go supplemental networking libraries")
      (description "This package provides supplemental Go networking libraries.")
      (home-page "https://go.googlesource.com/net")
      (license license:bsd-3))))

(define-public go-github-com-go-resty-resty-v2
  (package
    (name "go-github-com-go-resty-resty-v2")
    (version "2.7.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/go-resty/resty")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0ac293jsh71qg9sdxf6sizcakrmza0qabrw6x7mpsia2h8pxlf5x"))))
    (build-system go-build-system)
    (arguments
     '(#:tests? #f ; hangs?
       #:import-path "github.com/go-resty/resty/v2"))
    (propagated-inputs `(("go-golang-org-x-net" ,go-golang-org-x-net)))
    (home-page "https://github.com/go-resty/resty")
    (synopsis "News")
    (description
     "Package resty provides Simple HTTP and REST client library for Go.")
    (license license:expat)))

(define-public go-github-com-golang-mock
  (package
    (name "go-github-com-golang-mock")
    (version "1.6.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/golang/mock")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1hara8j0x431njjhqxfrg1png7xa1gbrpwza6ya4mwlx76hppap4"))))
    (build-system go-build-system)
    (arguments
     '(#:tests? #f ; needs client part of package?
       #:import-path "github.com/golang/mock/mockgen"
       #:unpack-path "github.com/golang/mock"))
    (propagated-inputs `(("go-golang-org-x-tools" ,go-golang-org-x-tools)
                         ("go-golang-org-xerrors" ,go-golang-org-x-xerrors)
                         ("go-golang-org-x-mod" ,go-golang-org-x-mod)))
    (home-page "https://github.com/golang/mock")
    (synopsis "gomock")
    (description
     "gomock is a mocking framework for the @url{http://golang.org/,Go programming
language}.  It integrates well with Go's built-in @code{testing} package, but
can be used in other contexts too.")
    (license license:asl2.0)))

(define-public go-github-com-hashicorp-errwrap
  (package
    (name "go-github-com-hashicorp-errwrap")
    (version "1.1.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/hashicorp/errwrap")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0p5wdz8p7dmwphmb33gwhy3iwci5k9wkfqmmfa6ay1lz0cqjwp7a"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/hashicorp/errwrap"))
    (home-page "https://github.com/hashicorp/errwrap")
    (synopsis "errwrap")
    (description
     "Package errwrap implements methods to formalize error wrapping in Go.")
    (license license:mpl2.0)))

(define-public go-github-com-hashicorp-go-multierror
  (package
    (name "go-github-com-hashicorp-go-multierror")
    (version "1.1.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/hashicorp/go-multierror")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0l4s41skdpifndn9s8y6s9vzgghdzg4z8z0lld9qjr28888wzp00"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/hashicorp/go-multierror"))
    (propagated-inputs `(("go-github-com-hashicorp-errwrap" ,go-github-com-hashicorp-errwrap)))
    (home-page "https://github.com/hashicorp/go-multierror")
    (synopsis "go-multierror")
    (description
     "@code{go-multierror} is a package for Go that provides a mechanism for
representing a list of @code{error} values as a single @code{error}.")
    (license license:mpl2.0)))

(define-public go-github-com-miekg-dns
  (package
    (name "go-github-com-miekg-dns")
    (version "1.1.50")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/miekg/dns")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1svvx9qamy3hy0ms8iwbisqjmfkbza0zljmds6091siq150ggmws"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/miekg/dns"))
    (propagated-inputs `(("go-golang-org-x-tools" ,go-golang-org-x-tools)
                         ("go-golang-org-x-sys" ,go-golang-org-x-sys)
                         ("go-golang-org-x-sync" ,go-golang-org-x-sync)
                         ("go-golang-org-x-net" ,go-golang-org-x-net)))
    (home-page "https://github.com/miekg/dns")
    (synopsis "Alternative (more granular) approach to a DNS library")
    (description
     "Package dns implements a full featured interface to the Domain Name System.
Both server- and client-side programming is supported.  The package allows
complete control over what is sent out to the DNS.  The API follows the
less-is-more principle, by presenting a small, clean interface.")
    (license license:bsd-3)))

(define-public go-github-com-ricochet2200-go-disk-usage-du
  (package
    (name "go-github-com-ricochet2200-go-disk-usage-du")
    (version "0.0.0-20210707232629-ac9918953285")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/ricochet2200/go-disk-usage")
                    (commit (go-version->git-ref version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0idg2hlr9lxaakwy1npfwbgjl7d38128b91wfk1qnnyxl0df5d80"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/ricochet2200/go-disk-usage/du"
       #:unpack-path "github.com/ricochet2200/go-disk-usage"))
    (home-page "https://github.com/ricochet2200/go-disk-usage")
    (synopsis #f)
    (description #f)
    (license license:unlicense)))

(define-public go-github-com-ssor-bom
  (package
    (name "go-github-com-ssor-bom")
    (version "0.0.0-20170718123548-6386211fdfcf")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/ssor/bom")
                    (commit (go-version->git-ref version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "09g5496ifwqxqclh2iw58plcwcz0sczlnxwqxzwmnl4shdl371ld"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/ssor/bom"))
    (home-page "https://github.com/ssor/bom")
    (synopsis "bom")
    (description "small tools for cleaning bom from byte array or reader")
    (license license:expat)))

(define-public go-github-com-vmihailenco-tagparser-v2
  (package
    (name "go-github-com-vmihailenco-tagparser-v2")
    (version "2.0.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/vmihailenco/tagparser")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "13arliaz3b4bja9jj7cr5ax4zvxaxm484fwrn0q6d6jjm1l35m1k"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/vmihailenco/tagparser/v2"))
    (home-page "https://github.com/vmihailenco/tagparser")
    (synopsis "Opinionated Golang tag parser")
    (description "Install:")
    (license license:bsd-2)))

(define-public go-github-com-emersion-go-message
  (package
    (name "go-github-com-emersion-go-message")
    (version "0.16.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/emersion/go-message")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1j5qdhsna28xcs843zsiccw700rld5hin466dl0n3a0ax1w13ay0"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/emersion/go-message"))
    (propagated-inputs `(("go-golang-org-x-text" ,go-golang-org-x-text)
                         ("go-github-com-emersion-go-textwrapper" ,go-github-com-emersion-go-textwrapper)))
    (home-page "https://github.com/emersion/go-message")
    (synopsis "go-message")
    (description
     "Package message implements reading and writing multipurpose messages.")
    (license license:expat)))

(define-public go-github-com-protonmail-go-imap
  (package
    (name "go-github-com-protonmail-go-imap")
    (version "1.0.6")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/ProtonMail/go-imap")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1cv74arkrzg4q1xpgp2ic28gmxpr69q220vicbgz62rkwihy7ja9"))))
    (build-system go-build-system)
    (arguments
     '(#:tests? #f ; use of internal package not allowed
       #:import-path "github.com/ProtonMail/go-imap"))
    (propagated-inputs `(("go-golang-org-x-text" ,go-golang-org-x-text)
                         ("go-github-com-emersion-go-imap" ,go-github-com-emersion-go-imap)
                         ("go-github-com-emersion-go-sasl" ,go-github-com-emersion-go-sasl)
                         ("go-github-com-emersion-go-message" ,go-github-com-emersion-go-message)))
    (home-page "https://github.com/ProtonMail/go-imap")
    (synopsis "go-imap")
    (description "Package imap implements IMAP4rev1
(@url{https://rfc-editor.org/rfc/rfc3501.html,RFC 3501}).")
    (license license:expat)))

(define-public go-github-com-emersion-go-textwrapper
  (package
    (name "go-github-com-emersion-go-textwrapper")
    (version "0.0.0-20200911093747-65d896831594")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/emersion/go-textwrapper")
                    (commit (go-version->git-ref version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1lh9d7zvj6gm1rr6sv5xlagklgx9d666hq5srd37a4sdcjkbiqmq"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/emersion/go-textwrapper"))
    (home-page "https://github.com/emersion/go-textwrapper")
    (synopsis "go-textwrapper")
    (description
     "This package provides a writer that wraps long text lines to a specified length.")
    (license license:expat)))

(define-public go-github-com-martinlindhe-base36
  (package
    (name "go-github-com-martinlindhe-base36")
    (version "1.1.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/martinlindhe/base36")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1hwv3gla4hgby9wfghazrrmrffhazj3mhbc7wsnig576w572c942"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/martinlindhe/base36"))
    (propagated-inputs `(("go-gopkg-in-yaml-v3" ,go-gopkg-in-yaml-v3)
                         ("go-github-com-pmezard-go-difflib" ,go-github-com-pmezard-go-difflib)
                         ("go-github-com-davecgh-go-spew" ,go-github-com-davecgh-go-spew)
                         ("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)))
    (home-page "https://github.com/martinlindhe/base36")
    (synopsis "About")
    (description
     "This package implements Base36 encoding and decoding, which is useful to
represent large integers in a case-insensitive alphanumeric way.")
    (license license:expat)))

(define-public go-github-com-protonmail-go-message
  (package
    (name "go-github-com-protonmail-go-message")
    (version "0.13.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/ProtonMail/go-message")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1b4ccjafzfydwjwcxv52ap9yqq3zvhylhfqg0g0hgd77h51f6gl4"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/ProtonMail/go-message"))
    (propagated-inputs `(("go-golang-org-x-text" ,go-golang-org-x-text)
                         ("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
                         ("go-github-com-martinlindhe-base36" ,go-github-com-martinlindhe-base36)
                         ("go-github-com-emersion-go-message" ,go-github-com-emersion-go-message)
                         ("go-github-com-emersion-go-textwrapper" ,go-github-com-emersion-go-textwrapper)
                         ("go-github-com-davecgh-go-spew" ,go-github-com-davecgh-go-spew)))
    (home-page "https://github.com/ProtonMail/go-message")
    (synopsis "go-message")
    (description
     "Package message implements reading and writing multipurpose messages.")
    (license license:expat)))

(define-public go-github-com-protonmail-bcrypt
  (package
    (name "go-github-com-protonmail-bcrypt")
    (version "0.0.0-20211005172633-e235017c1baf")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/ProtonMail/bcrypt")
                    (commit (go-version->git-ref version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0znxsia69c2p4vgj9qckqq3p06rg2b5qzdm7p7svs3x46bp28s3s"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/ProtonMail/bcrypt"))
    (home-page "https://github.com/ProtonMail/bcrypt")
    (synopsis "github.com/ProtonMail/bcrypt")
    (description
     "This package provides a golang implementation of the bcrypt hash algorithm.  It
is a fork of
@url{https://github.com/jameskeane/bcrypt,github.com/jameskeane/bcrypt}.")
    (license license:bsd-3)))

(define peroxide-x-net-broken
  (package
    (name "peroxide")
    (version "0.5.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/ljanyst/peroxide")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "12c6mdn2anq4pk0z7x16z897w9hvr9gi8z8ai6kfg3cdhs5zb5p8"))))
    (build-system go-build-system)
    (arguments
      (list #:tests? #f ; no tests?
            #:import-path "github.com/ljanyst/peroxide"
            #:phases
            #~(modify-phases %standard-phases
                (replace 'build
                  (lambda _
                    (with-directory-excursion "src/github.com/ljanyst/peroxide/cmd/peroxide"
                      (invoke "go" "build" "-v" "-ldflags=-s -w"))
                    (with-directory-excursion "src/github.com/ljanyst/peroxide/cmd/peroxide-cfg"
                      (invoke "go" "build" "-v" "-ldflags=-s -w"))))
                (add-before 'install 'install-bin
                  (lambda _
                    (install-file "src/github.com/ljanyst/peroxide/cmd/peroxide/peroxide"
                                  (string-append #$output "/sbin"))
                    (install-file "src/github.com/ljanyst/peroxide/cmd/peroxide-cfg/peroxide-cfg"
                                  (string-append #$output "/sbin"))
                    (invoke "mv" "src/github.com/ljanyst/peroxide/config.example.yaml"
                            "src/github.com/ljanyst/peroxide/config.conf")
                    (install-file "src/github.com/ljanyst/peroxide/config.conf"
                                  (string-append #$output "/etc")))))))
    (propagated-inputs `(("go-github-com-protonmail-bcrypt" ,go-github-com-protonmail-bcrypt)
                         ("go-github-com-protonmail-go-message" ,go-github-com-protonmail-go-message)
                         ("go-github-com-protonmail-go-imap" ,go-github-com-protonmail-go-imap)
                         ("go-gopkg-in-yaml-v3" ,go-gopkg-in-yaml-v3)
                         ("go-gopkg-in-yaml-v2" ,go-gopkg-in-yaml-v2)
                         ("go-gopkg-in-check-v1" ,go-gopkg-in-check-v1)
                         ("go-golang-org-x-text" ,go-golang-org-x-text)
                         ("go-golang-org-x-sys" ,go-golang-org-x-sys)
                         ("go-golang-org-x-net" ,go-golang-org-x-net)
                         ("go-golang-org-x-crypto" ,go-golang-org-x-crypto)
                         ("go-golang-org-x-term" ,go-golang-org-x-term)
                         ("go-go-etcd-io-bbolt" ,go-go-etcd-io-bbolt)
                         ("go-github-com-vmihailenco-msgpack-v5" ,go-github-com-vmihailenco-msgpack-v5)
                         ("go-github-com-stretchr-testify" ,go-github-com-stretchr-testify)
                         ("go-github-com-ssor-bom" ,go-github-com-ssor-bom)
                         ("go-github-com-sirupsen-logrus" ,go-github-com-sirupsen-logrus)
                         ("go-github-com-ricochet2200-go-disk-usage-du" ,go-github-com-ricochet2200-go-disk-usage-du)
                         ("go-github-com-pkg-errors" ,go-github-com-pkg-errors)
                         ("go-github-com-olekukonko-tablewriter" ,go-github-com-olekukonko-tablewriter)
                         ("go-github-com-miekg-dns" ,go-github-com-miekg-dns)
                         ("go-github-com-mattn-go-runewidth" ,go-github-com-mattn-go-runewidth)
                         ("go-github-com-mattn-go-isatty" ,go-github-com-mattn-go-isatty)
                         ("go-github-com-kr-text" ,go-github-com-kr-text)
                         ("go-github-com-jaytaylor-html2text" ,go-github-com-jaytaylor-html2text)
                         ("go-github-com-hashicorp-go-multierror" ,go-github-com-hashicorp-go-multierror)
                         ("go-github-com-google-uuid" ,go-github-com-google-uuid)
                         ("go-github-com-google-go-cmp" ,go-github-com-google-go-cmp)
                         ("go-github-com-golang-mock" ,go-github-com-golang-mock)
                         ("go-github-com-go-resty-resty-v2" ,go-github-com-go-resty-resty-v2)
                         ("go-github-com-ghodss-yaml" ,go-github-com-ghodss-yaml)
                         ("go-github-com-emersion-go-vcard" ,go-github-com-emersion-go-vcard)
                         ("go-github-com-emersion-go-textwrapper" ,go-github-com-emersion-go-textwrapper)
                         ("go-github-com-emersion-go-smtp" ,go-github-com-emersion-go-smtp)
                         ("go-github-com-emersion-go-sasl" ,go-github-com-emersion-go-sasl)
                         ("go-github-com-emersion-go-imap-unselect" ,go-github-com-emersion-go-imap-unselect)
                         ("go-github-com-emersion-go-imap-quota" ,go-github-com-emersion-go-imap-quota)
                         ("go-github-com-emersion-go-imap-move" ,go-github-com-emersion-go-imap-move)
                         ("go-github-com-emersion-go-imap-appendlimit" ,go-github-com-emersion-go-imap-appendlimit)
                         ("go-github-com-puerkitobio-goquery" ,go-github-com-puerkitobio-goquery)
                         ("go-github-com-protonmail-gopenpgp-v2" ,go-github-com-protonmail-gopenpgp-v2)
                         ("go-github-com-protonmail-go-vcard" ,go-github-com-protonmail-go-vcard)
                         ("go-github-com-protonmail-go-srp" ,go-github-com-protonmail-go-srp)
                         ("go-github-com-protonmail-go-rfc5322" ,go-github-com-protonmail-go-rfc5322)
                         ("go-github-com-protonmail-go-crypto" ,go-github-com-protonmail-go-crypto)))
    (home-page "https://github.com/ljanyst/peroxide")
    (synopsis "peroxide")
    (description
     "Peroxide is a fork of the
@url{https://github.com/ProtonMail/proton-bridge,ProtonMail bridge}.  Its goal
is to be much like @url{https://github.com/emersion/hydroxide,Hydroxide} except
with as much re-use of the upstream code as possible.  The re-use ensures that
the upstream changes to the service APIs can be merged in as fast and as
efficiently as possible.  At the same time, Peroxide aims to:")
    (license license:gpl3)))

(define use-x-net-old
  (package-input-rewriting/spec `(("go-golang-org-x-net" . ,(const go-golang-org-x-net-old)))))

(define-public peroxide
  (use-x-net-old peroxide-x-net-broken))

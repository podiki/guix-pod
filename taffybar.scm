;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2021 John Kehayias <john.kehayias@protonmail.com>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (taffybar) ;(gnu packages haskell-gi)
  #:use-module (guix packages)
  #:use-module (guix utils) ; for gobject hidden package
  #:use-module (guix build-system gnu) ; ditto
  #:use-module (gnu packages gnome) ; dittos
  #:use-module (guix download)
  #:use-module (guix git-download) ; for snixembed only
  #:use-module (guix build-system haskell)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages haskell-check)
  #:use-module (gnu packages haskell-web)
  #:use-module (gnu packages haskell-xyz)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages wm)
  #:use-module (gnu packages xorg))

(define-public ghc-xml-helpers
  (package
    (name "ghc-xml-helpers")
    (version "1.0.0")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://hackage.haskell.org/package/xml-helpers/xml-helpers-"
             version
             ".tar.gz"))
       (sha256
        (base32 "0rrk0j7m8ws86hbjw0l4ryq4m9i8llhsag2sfisy5r1iv2zwa0lv"))))
    (build-system haskell-build-system)
    (inputs (list ghc-xml))
    (home-page "https://github.com/acw/xml-helpers")
    (synopsis "Some useful helper functions for the xml library.")
    (description
     "Included are some folds and maps I've found useful in parsing XML data.")
    (license license:bsd-3)))

(define-public ghc-xdg-desktop-entry
  (package
    (name "ghc-xdg-desktop-entry")
    (version "0.1.1.1")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://hackage.haskell.org/package/xdg-desktop-entry/xdg-desktop-entry-"
             version
             ".tar.gz"))
       (sha256
        (base32 "0xlniirgj01v02dp6wx8iw038p4mx2pa3rmwfv3g7k5raa7gzapb"))))
    (build-system haskell-build-system)
    (inputs (list ghc-configfile ghc-either ghc-multimap ghc-safe))
    (home-page "http://hackage.haskell.org/package/xdg-desktop-entry")
    (synopsis "Parse files conforming to the xdg desktop entry spec")
    (description "")
    (license license:bsd-3)))

(define-public ghc-onetuple
  (package
    (name "ghc-onetuple")
    (version "0.2.2.1")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://hackage.haskell.org/package/OneTuple/OneTuple-"
             version
             ".tar.gz"))
       (sha256
        (base32 "15ls6kkf953288q7rsc49bvw467ll4nq28hvsgbaazdn7hf75ixc"))))
    (build-system haskell-build-system)
    (arguments
     `(#:cabal-revision
       ("1" "03mygfz7lv6h0i30bq2grvmahbg9j7a36mc0wls2nr81dv9p19s7")))
    (home-page "http://hackage.haskell.org/package/OneTuple")
    (synopsis "Singleton Tuple")
    (description
     "This package provides a singleton tuple data type . > data OneTuple a = OneTuple a
. Note: it's not a @newtype@")
    (license license:bsd-3)))

(define-public ghc-tuple
  (package
    (name "ghc-tuple")
    (version "0.3.0.2")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://hackage.haskell.org/package/tuple/tuple-"
             version
             ".tar.gz"))
       (sha256
        (base32 "094nx29aahyrvbcn7yca9zs2a5rxz1is7510w1q43rpvza7hdjrg"))))
    (build-system haskell-build-system)
    (inputs (list ghc-onetuple))
    (home-page "http://hackage.haskell.org/package/tuple")
    (synopsis "Various functions on tuples")
    (description
     "Various useful functions on tuples, overloaded on tuple size.")
    (license license:bsd-3)))

(define-public ghc-hspec-wai
  (package
    (name "ghc-hspec-wai")
    (version "0.11.1")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://hackage.haskell.org/package/hspec-wai/hspec-wai-"
             version
             ".tar.gz"))
       (sha256
        (base32 "03wiksic5y9a2g6a86nsxrnajdgdvpv17w02h5qla0zp9zs6pa1j"))))
    (build-system haskell-build-system)
    (inputs
     (list ghc-quickcheck
           ghc-base-compat
           ghc-case-insensitive
           ghc-hspec-core
           ghc-hspec-expectations
           ghc-http-types
           ghc-wai
           ghc-wai-extra))
    (native-inputs (list ghc-hspec hspec-discover))
    (home-page "https://github.com/hspec/hspec-wai#readme")
    (synopsis "Experimental Hspec support for testing WAI applications")
    (description "Experimental Hspec support for testing WAI applications")
    (license license:expat)))

(define-public ghc-scotty
  (package
    (name "ghc-scotty")
    (version "0.12")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://hackage.haskell.org/package/scotty/scotty-"
             version
             ".tar.gz"))
       (sha256
        (base32 "1lpggpdzgjk23mq7aa64yylds5dbm4ynhcvbarqihjxabvh7xmz1"))))
    (build-system haskell-build-system)
    (inputs
     (list ghc-aeson
           ghc-base-compat-batteries
           ghc-blaze-builder
           ghc-case-insensitive
           ghc-data-default-class
           ghc-fail
           ghc-http-types
           ghc-monad-control
           ghc-nats
           ghc-network
           ghc-regex-compat
           ghc-transformers-base
           ghc-transformers-compat
           ghc-wai
           ghc-wai-extra
           ghc-warp))
    (native-inputs (list ghc-async ghc-hspec ghc-hspec-wai ghc-lifted-base
                         hspec-discover))
    (arguments
     `(#:cabal-revision
       ("6" "15gwvx9gdk4vxh1x2n5xvnrix8m0wl96a4aqbdmdfrka43sywfma")))
    (home-page "https://github.com/scotty-web/scotty")
    (synopsis
     "Haskell web framework inspired by Ruby's Sinatra, using WAI and Warp")
    (description
     "A Haskell web framework inspired by Ruby's Sinatra, using WAI and Warp. . @ &#123;-&#35;
LANGUAGE OverloadedStrings &#35;-&#125; . import Web.Scotty . import Data.Monoid (mconcat)
. main = scotty 3000 $ &#32;&#32;get &#34;/:word&#34; $ do &#32;&#32;&#32;&#32;beam <-
param &#34;word&#34; &#32;&#32;&#32;&#32;html $ mconcat [&#34;&#60;h1&#62;Scotty, &#34;,
beam, &#34; me up!&#60;/h1&#62;&#34;] @ . . Scotty is the cheap and cheerful way to write
RESTful, declarative web applications. . * A page is as simple as defining the verb, url
pattern, and Text content. . * It is template-language agnostic. Anything that returns a
Text value will do. . * Conforms to WAI Application interface. . * Uses very fast Warp
webserver by default. . As for the name: Sinatra + Warp = Scotty. . [WAI]
<http://hackage.haskell.org/package/wai> . [Warp]
<http://hackage.haskell.org/package/warp>")
    (license license:bsd-3)))

(define-public ghc-time-units
  (package
    (name "ghc-time-units")
    (version "1.0.0")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://hackage.haskell.org/package/time-units/time-units-"
             version
             ".tar.gz"))
       (sha256
        (base32 "16g0i6r6vj9w4lbn12jqrhgbbjjca8wbzq6546dz08aks1yrk0g1"))))
    (build-system haskell-build-system)
    (home-page "https://github.com/acw/time-units")
    (synopsis "A basic library for defining units of time as types.")
    (description
     "In many cases, it is useful (either for error checking or documentation reasons) to
define input and output types as having a particular unit of time.  In addition, by
creating a type class defining type units, this library should make it easier to separate
the units of time the developer wants to think in versus the units of time the library
author wants to think in.")
    (license license:bsd-3)))

(define-public ghc-rate-limit
  (package
    (name "ghc-rate-limit")
    (version "1.4.2")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://hackage.haskell.org/package/rate-limit/rate-limit-"
             version
             ".tar.gz"))
       (sha256
        (base32 "0zb19vwzyj1vg890776r3bprmjzhs9kr2r1vqa42nxv9nvwvnljm"))))
    (build-system haskell-build-system)
    (inputs (list ghc-time-units))
    (home-page "https://github.com/acw/rate-limit")
    (synopsis "A basic library for rate-limiting IO actions.")
    (description
     "In many cases, it is useful, necessary, or simply nice to limit how frequently you
perform some action.  For example, you may want to limit how often your program makes a
request of some web site.  This library is intended as a general-purpose mechanism for
rate-limiting IO actions.")
    (license license:bsd-3)))

(define-public ghc-multimap
  (package
    (name "ghc-multimap")
    (version "1.2.1")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://hackage.haskell.org/package/multimap/multimap-"
             version
             ".tar.gz"))
       (sha256
        (base32 "0d3l5q4yvmywl6i9ip96zz0fvhjdh00mfbbniphbjxsi8wlwack3"))))
    (build-system haskell-build-system)
    (home-page "http://hub.darcs.net/scravy/multimap")
    (synopsis "A multimap.")
    (description
     "This is a simple implementation of a multimap, based on \"Data.Map\".  [@v1.1@] @!@ had
its arguments flipped. Fixed. Also added @fromMap@.  [@v1.2@] Added \"Data.SetMap\",
renamed @Multimap@ to \"Data.MultiMap\". Fixed the type of @delete@. Derive instances for
@Data@ and @Typeable@.  [@v1.2.1@] Fixed typos in the documentation.")
    (license license:expat)))

(define-public ghc-bytestring-to-vector
  (package
    (name "ghc-bytestring-to-vector")
    (version "0.3.0.1")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://hackage.haskell.org/package/bytestring-to-vector/bytestring-to-vector-"
             version
             ".tar.gz"))
       (sha256
        (base32 "0ji836sl72wlhy6yay11kl86w0nrcdc1lafbi94bx9c8rpf5pyyc"))))
    (build-system haskell-build-system)
    (inputs (list ghc-vector))
    (native-inputs (list ghc-byteorder ghc-quickcheck))
    (home-page "https://github.com/sheyll/bytestring-to-vector")
    (synopsis "Convert between ByteString and Vector.Storable without copying")
    (description
     "This library allows conversion between the types from @Data.ByteString@ (package
@bytestring@) and @Data.Vector.Storable@ (package @vector@) without copying the underlying
data.  This is useful, for example, when @ByteString@ IO produces or consumes vectors of
numbers in native byte order.  The conversion relies on the fact that @ByteString@ and
@Vector@ use their respective @ForeignPtr@s in compatible ways.  This library is a fork
of the @spool@ package written by Keegan McAllister.")
    (license license:bsd-3)))

(define-public ghc-primitive-unaligned
  (package
    (name "ghc-primitive-unaligned")
    (version "0.1.1.1")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://hackage.haskell.org/package/primitive-unaligned/primitive-unaligned-"
             version
             ".tar.gz"))
       (sha256
        (base32 "1f3a46d9dr7x1k8b6ixnp9jzxkppx3g27qsxq4f244ndnf2jnchl"))))
    (build-system haskell-build-system)
    (inputs (list ghc-primitive))
    (home-page "https://github.com/haskell-primitive/primitive-unaligned")
    (synopsis "Unaligned access to primitive arrays")
    (description
     "Unaligned access to primitive arrays.  The offsets are given in bytes rather than
elements.")
    (license license:bsd-3)))

(define-public ghc-byte-order
  (package
    (name "ghc-byte-order")
    (version "0.1.2.0")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://hackage.haskell.org/package/byte-order/byte-order-"
             version
             ".tar.gz"))
       (sha256
        (base32 "1nnq4qmqmkv61xcyxrh14s6hg7rbnjkna6mwlrqh1rr59pikn45w"))))
    (build-system haskell-build-system)
    (inputs (list ghc-primitive ghc-primitive-unaligned))
    (home-page "https://github.com/andrewthad/byte-order")
    (synopsis "Portable big-endian and little-endian conversions")
    (description
     "This library provides an interface to portably work with byte arrays whose contents are
known to be of a fixed endianness.  There are two ways to use this module. See the
`System.ByteOrder` module for more documentation.")
    (license license:bsd-3)))

(define-public ghc-status-notifier-item
  (package
    (name "ghc-status-notifier-item")
    (version "0.3.1.0")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://hackage.haskell.org/package/status-notifier-item/status-notifier-item-"
             version
             ".tar.gz"))
       (sha256
        (base32 "1x3zqa2b9vl5mirfbh2bmyali47jpfcqsw4xxgbmsgz9jiffpda9"))))
    (build-system haskell-build-system)
    (inputs
     (list ghc-byte-order
           ghc-bytestring-to-vector
           ghc-dbus
           ghc-hslogger
           ghc-lens
           ghc-vector
           ghc-optparse-applicative
           ghc-dbus-hslogger))
    (home-page "https://github.com/IvanMalison/status-notifier-item#readme")
    (synopsis
     "A wrapper over the StatusNotifierItem/libappindicator dbus specification")
    (description
     "Please see the README on Github at <https://github.com/IvanMalison/status-notifier-item#readme>")
    (license license:bsd-3)))

(define-public ghc-gtk-strut
  (package
    (name "ghc-gtk-strut")
    (version "0.1.3.0")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://hackage.haskell.org/package/gtk-strut/gtk-strut-"
             version
             ".tar.gz"))
       (sha256
        (base32 "19p3w1zvnaazfd01yy4cl00sl53xc7kqgqhsw7l3psadmwk6x4w1"))))
    (build-system haskell-build-system)
    (inputs (list ghc-gi-gdk ghc-gi-gtk))
    (home-page "https://github.com/IvanMalison/gtk-strut#readme")
    (synopsis "Libary for creating strut windows with gi-gtk")
    (description
     "Please see the README on Github at <https://github.com/IvanMalison/gtk-strut#readme>")
    (license license:bsd-3)))

(define-public ghc-gi-dbusmenu
  (package
    (name "ghc-gi-dbusmenu")
    (version "0.4.10")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://hackage.haskell.org/package/gi-dbusmenu/gi-dbusmenu-"
             version
             ".tar.gz"))
       (sha256
        (base32 "05q3sijfsma2rinyw1rb7z4fphdqakbg18zlpf8jpv89c1329d3q"))))
    (build-system haskell-build-system)
    (inputs
     (list ghc-haskell-gi-base
           ghc-haskell-gi
           ghc-haskell-gi-overloading
           ghc-gi-glib
           ghc-gi-gobject
           gobject-introspection
           libdbusmenu))
    (native-inputs (list pkg-config))
    (home-page "https://github.com/haskell-gi/haskell-gi")
    (synopsis "Dbusmenu bindings")
    (description "Bindings for libdbusmenu, autogenerated by haskell-gi.")
    (license license:lgpl2.1)))

(define-public ghc-gi-dbusmenugtk3
  (package
    (name "ghc-gi-dbusmenugtk3")
    (version "0.4.11")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://hackage.haskell.org/package/gi-dbusmenugtk3/gi-dbusmenugtk3-"
             version
             ".tar.gz"))
       (sha256
        (base32 "1ipq8irzqswpak6zip9ghpkvk29mwnym8d70md3an0idlq0gcnw6"))))
    (build-system haskell-build-system)
    (inputs
     (list gdk-pixbuf
           ghc-haskell-gi-base
           ghc-haskell-gi
           ghc-haskell-gi-overloading
           ghc-gi-glib
           ghc-gi-gobject
           ghc-gi-gtk
           ghc-gi-dbusmenu
           ghc-gi-atk
           ghc-gi-gdk
           ghc-gi-gdkpixbuf
           gobject-introspection
           gtk+
           libdbusmenu))
    (native-inputs (list pkg-config))
    (home-page "https://github.com/haskell-gi/haskell-gi")
    (synopsis "DbusmenuGtk bindings")
    (description "Bindings for libdbusgtk3, autogenerated by haskell-gi.")
    (license license:lgpl2.1)))

(define-public ghc-gtk-sni-tray
  (package
    (name "ghc-gtk-sni-tray")
    (version "0.1.8.0")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://hackage.haskell.org/package/gtk-sni-tray/gtk-sni-tray-"
             version
             ".tar.gz"))
       (sha256
        (base32 "0b8b8hvsv60zvnjidqm1qchxdgqdf5gcmm5pz8fvrvcjqq27b1xa"))))
    (build-system haskell-build-system)
    (inputs
     (list ghc-dbus
           ghc-enclosed-exceptions
           ghc-gi-cairo
           ghc-gi-cairo-connector
           ghc-gi-cairo-render
           ghc-gi-dbusmenugtk3
           ghc-gi-gdk
           ghc-gi-gdkpixbuf
           ghc-gi-glib
           ghc-gi-gtk
           ghc-gtk-strut
           ghc-haskell-gi
           ghc-haskell-gi-base
           ghc-hslogger
           ghc-status-notifier-item
           ghc-transformers-base
           ghc-dbus-hslogger
           ghc-optparse-applicative
           gtk+))
    (native-inputs (list pkg-config))
    (home-page "https://github.com/IvanMalison/gtk-sni-tray#readme")
    (synopsis "A standalone StatusNotifierItem/AppIndicator tray")
    (description
     "Please see the README on Github at <https://github.com/IvanMalison/gtk-sni-tray#readme>")
    (license license:bsd-3)))

(define-public ghc-gi-gtk-hs
  (package
    (name "ghc-gi-gtk-hs")
    (version "0.3.11")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://hackage.haskell.org/package/gi-gtk-hs/gi-gtk-hs-"
             version
             ".tar.gz"))
       (sha256
        (base32 "0scbipy7086pv1xc57vmq48wcx3p7wbznwjcn99dqw4gf9yrqwwz"))))
    (build-system haskell-build-system)
    (inputs
     (list ghc-base-compat
           ghc-haskell-gi-base
           ghc-gi-glib
           ghc-gi-gobject
           ghc-gi-gdk
           ghc-gi-gtk
           ghc-gi-gdkpixbuf))
    (home-page "https://github.com/haskell-gi/haskell-gi")
    (synopsis
     "A wrapper for gi-gtk, adding a few more idiomatic API parts on top")
    (description
     "A wrapper for gi-gtk, adding a few more idiomatic API parts on top")
    (license license:lgpl2.1)))

(define-public ghc-gi-atk
  (package
    (name "ghc-gi-atk")
    (version "2.0.24")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://hackage.haskell.org/package/gi-atk/gi-atk-"
             version
             ".tar.gz"))
       (sha256
        (base32 "1xhjzjcdbnfk7b6r035ch3plpgk9yk2qawq7iffc47y67dxhcd6b"))))
    (build-system haskell-build-system)
    (inputs
     (list atk
           ghc-haskell-gi-base
           ghc-haskell-gi
           ghc-haskell-gi-overloading
           ghc-gi-gobject
           ghc-gi-glib
           gobject-introspection))
    (native-inputs (list pkg-config))
    (home-page "https://github.com/haskell-gi/haskell-gi")
    (synopsis "Atk bindings")
    (description "Bindings for Atk, autogenerated by haskell-gi.")
    (license license:lgpl2.1)))

(define-public ghc-gi-gtk
  (package
    (name "ghc-gi-gtk")
    (version "3.0.38")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://hackage.haskell.org/package/gi-gtk/gi-gtk-"
             version
             ".tar.gz"))
       (sha256
        (base32 "0xnz5969v9nz6llg7856zdn3pcn1llvr1p0jl8vxk4n5wrwgqjms"))))
    (build-system haskell-build-system)
    (inputs
     (list ghc-haskell-gi-base
           ghc-haskell-gi
           ghc-haskell-gi-overloading
           ghc-gi-cairo
           ghc-gi-pango
           ghc-gi-gio
           ghc-gi-gdkpixbuf
           ghc-gi-gdk
           ghc-gi-gobject
           ghc-gi-glib
           ghc-gi-atk
           gobject-introspection
           gtk+))
    (native-inputs (list pkg-config))
    (home-page "https://github.com/haskell-gi/haskell-gi")
    (synopsis "Gtk bindings")
    (description "Bindings for Gtk, autogenerated by haskell-gi.")
    (license license:lgpl2.1)))

(define-public ghc-gi-xlib
  (package
    (name "ghc-gi-xlib")
    (version "2.0.11")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://hackage.haskell.org/package/gi-xlib/gi-xlib-"
             version
             ".tar.gz"))
       (sha256
        (base32 "0l6xr26asmy3rvzi5lazkfpik1n41v9a7bg2pypssc26130amp1f"))))
    (build-system haskell-build-system)
    (inputs
     (list ghc-haskell-gi-base ghc-haskell-gi ghc-haskell-gi-overloading
           gobject-introspection libx11))
    (native-inputs (list pkg-config))
    (home-page "https://github.com/haskell-gi/haskell-gi")
    (synopsis "xlib bindings")
    (description "Bindings for xlib, autogenerated by haskell-gi.")
    (license license:lgpl2.1)))

(define-public ghc-gi-gdkx11
  (package
    (name "ghc-gi-gdkx11")
    (version "3.0.12")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://hackage.haskell.org/package/gi-gdkx11/gi-gdkx11-"
             version
             ".tar.gz"))
       (sha256
        (base32 "0lx5c13i4g4jy47zpkhd3nn714d7n2ljqib4p4ylgs9cyj145hd4"))))
    (build-system haskell-build-system)
    (inputs
     (list ghc-haskell-gi-base
           ghc-haskell-gi
           ghc-haskell-gi-overloading
           ghc-gi-gobject
           ghc-gi-gdk
           ghc-gi-gio
           ghc-gi-cairo
           ghc-gi-xlib
           gobject-introspection
           gtk+))
    (native-inputs (list pkg-config))
    (home-page "https://github.com/haskell-gi/haskell-gi")
    (synopsis "GdkX11 bindings")
    (description "Bindings for GdkX11, autogenerated by haskell-gi.")
    (license license:lgpl2.1)))

(define-public ghc-gi-gmodule
  (package
    (name "ghc-gi-gmodule")
    (version "2.0.2")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://hackage.haskell.org/package/gi-gmodule/gi-gmodule-"
             version
             ".tar.gz"))
       (sha256
        (base32 "1jbplvicc6jsjcz8gqkiq71b8cx57m010wbq2ilirv4rif90ggnx"))))
    (build-system haskell-build-system)
    (inputs
     (list ghc-haskell-gi-base
           ghc-haskell-gi
           ghc-haskell-gi-overloading
           ghc-gi-glib
           gobject-introspection))
    (native-inputs (list pkg-config))
    (home-page "https://github.com/haskell-gi/haskell-gi")
    (synopsis "GModule bindings")
    (description "Bindings for GModule, autogenerated by haskell-gi.")
    (license license:lgpl2.1)))

(define-public ghc-gi-gdkpixbuf
  (package
    (name "ghc-gi-gdkpixbuf")
    (version "2.0.27")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://hackage.haskell.org/package/gi-gdkpixbuf/gi-gdkpixbuf-"
             version
             ".tar.gz"))
       (sha256
        (base32 "0p2b4yvfwcj3bpn9r85v6xjlw6rr50kdca0kan45mv0aia0nyg33"))))
    (build-system haskell-build-system)
    (inputs
     (list gdk-pixbuf
           ghc-haskell-gi-base
           ghc-haskell-gi
           ghc-haskell-gi-overloading
           ghc-gi-gio
           ghc-gi-gobject
           ghc-gi-glib
           ghc-gi-gmodule
           gobject-introspection))
    (native-inputs (list pkg-config))
    (home-page "https://github.com/haskell-gi/haskell-gi")
    (synopsis "GdkPixbuf bindings")
    (description "Bindings for GdkPixbuf, autogenerated by haskell-gi.")
    (license license:lgpl2.1)))

(define-public ghc-gi-gio
  (package
    (name "ghc-gi-gio")
    (version "2.0.29")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://hackage.haskell.org/package/gi-gio/gi-gio-"
             version
             ".tar.gz"))
       (sha256
        (base32 "14kh1qdayi55flf2108ivq7sc1k9qd8dish19jbdij198hsjgria"))))
    (build-system haskell-build-system)
    (inputs
     (list ghc-haskell-gi-base
           ghc-haskell-gi
           ghc-haskell-gi-overloading
           ghc-gi-gobject
           ghc-gi-glib
           gobject-introspection))
    (native-inputs (list pkg-config))
    (home-page "https://github.com/haskell-gi/haskell-gi")
    (synopsis "Gio bindings")
    (description "Bindings for Gio, autogenerated by haskell-gi.")
    (license license:lgpl2.1)))

(define-public ghc-gi-harfbuzz
  (package
    (name "ghc-gi-harfbuzz")
    (version "0.0.5")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://hackage.haskell.org/package/gi-harfbuzz/gi-harfbuzz-"
             version
             ".tar.gz"))
       (sha256
        (base32 "1kngcm03596cqz4djll1snmif2wdpkih0awkavcl2m63xcd86m4z"))))
    (build-system haskell-build-system)
    (inputs
     (list freetype
           ghc-haskell-gi-base
           ghc-haskell-gi
           ghc-haskell-gi-overloading
           ghc-gi-gobject
           ghc-gi-glib
           gobject-introspection
           harfbuzz))
    (native-inputs (list pkg-config))
    (home-page "https://github.com/haskell-gi/haskell-gi")
    (synopsis "HarfBuzz bindings")
    (description "Bindings for HarfBuzz, autogenerated by haskell-gi.")
    (license license:lgpl2.1)))

(define-public ghc-gi-glib
  (package
    (name "ghc-gi-glib")
    (version "2.0.26")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://hackage.haskell.org/package/gi-glib/gi-glib-"
             version
             ".tar.gz"))
       (sha256
        (base32 "0i38ch8giqs92kkfzyw4wlz8y0r5kn2h94b6y33nj4ja3ggrg1qm"))))
    (build-system haskell-build-system)
    (inputs
     (list ghc-haskell-gi-base ghc-haskell-gi ghc-haskell-gi-overloading
           gobject-introspection))
    (native-inputs (list pkg-config))
    (home-page "https://github.com/haskell-gi/haskell-gi")
    (synopsis "GLib bindings")
    (description "Bindings for GLib, autogenerated by haskell-gi.")
    (license license:lgpl2.1)))

(define-public ghc-gi-gobject
  (package
    (name "ghc-gi-gobject")
    (version "2.0.27")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://hackage.haskell.org/package/gi-gobject/gi-gobject-"
             version
             ".tar.gz"))
       (sha256
        (base32 "1xfw8jg81pbhgb20kw2jvhbxcs8d0sl4zf3dsar9sy7gl4lgwh0g"))))
    (build-system haskell-build-system)
    (inputs
     (list ghc-haskell-gi-base
           ghc-haskell-gi
           ghc-haskell-gi-overloading
           ghc-gi-glib
           gobject-introspection))
    (native-inputs (list pkg-config))
    (home-page "https://github.com/haskell-gi/haskell-gi")
    (synopsis "GObject bindings")
    (description "Bindings for GObject, autogenerated by haskell-gi.")
    (license license:lgpl2.1)))

(define-public ghc-gi-pango
  (package
    (name "ghc-gi-pango")
    (version "1.0.25")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://hackage.haskell.org/package/gi-pango/gi-pango-"
             version
             ".tar.gz"))
       (sha256
        (base32 "1m1n9jcpr8j4k7212yv4asm62w0pp9wlikl164xfgv8yfa3gh7s0"))))
    (build-system haskell-build-system)
    (inputs
     (list ghc-haskell-gi-base
           ghc-haskell-gi
           ghc-haskell-gi-overloading
           ghc-gi-gobject
           ghc-gi-glib
           ghc-gi-harfbuzz
           gobject-introspection+cairo
           pango))
    (native-inputs (list pkg-config))
    (home-page "https://github.com/haskell-gi/haskell-gi")
    (synopsis "Pango bindings")
    (description "Bindings for Pango, autogenerated by haskell-gi.")
    (license license:lgpl2.1)))

(define-public ghc-gi-gdk
  (package
    (name "ghc-gi-gdk")
    (version "3.0.25")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://hackage.haskell.org/package/gi-gdk/gi-gdk-"
             version
             ".tar.gz"))
       (sha256
        (base32 "1flgsm7sqsfp53pqbc8fv9pnsc84qnjmhircsqnx8jvkg5iyvxnx"))))
    (build-system haskell-build-system)
    (inputs
     (list gdk-pixbuf
           ghc-haskell-gi-base
           ghc-haskell-gi
           ghc-haskell-gi-overloading
           ghc-gi-cairo
           ghc-gi-pango
           ghc-gi-gio
           ghc-gi-gdkpixbuf
           ghc-gi-gobject
           ghc-gi-glib
           gobject-introspection
           gtk+))
    (native-inputs (list pkg-config))
    (home-page "https://github.com/haskell-gi/haskell-gi")
    (synopsis "Gdk bindings")
    (description "Bindings for Gdk, autogenerated by haskell-gi.")
    (license license:lgpl2.1)))

(define-public ghc-gi-cairo-render
  (package
    (name "ghc-gi-cairo-render")
    (version "0.1.1")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://hackage.haskell.org/package/gi-cairo-render/gi-cairo-render-"
             version
             ".tar.gz"))
       (sha256
        (base32 "09gxykx633xvwcqx1cjl9kj2748jdq49ayy6z5p3hz7bxqlna952"))))
    (build-system haskell-build-system)
    (inputs (list cairo ghc-c2hs ghc-utf8-string ghc-haskell-gi-base))
    (native-inputs (list pkg-config))
    (home-page "https://github.com/cohomology/gi-cairo-render")
    (synopsis "GI friendly Binding to the Cairo library.")
    (description
     "Cairo is a library to render high quality vector graphics.  There exist various backends
that allows rendering to Gtk windows, PDF, PS, PNG and SVG documents, amongst others.")
    (license license:bsd-3)))

(define-public ghc-gi-cairo-connector
  (package
    (name "ghc-gi-cairo-connector")
    (version "0.1.1")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://hackage.haskell.org/package/gi-cairo-connector/gi-cairo-connector-"
             version
             ".tar.gz"))
       (sha256
        (base32 "0jkca1x0dlyfwyywn6gmsnxhyad3j7vfwa0c4hpvx8k8m350dn3c"))))
    (build-system haskell-build-system)
    (inputs (list ghc-haskell-gi-base ghc-gi-cairo ghc-gi-cairo-render))
    (home-page "https://github.com/cohomology/gi-cairo-render")
    (synopsis "GI friendly Binding to the Cairo library.")
    (description
     "This library contains glue code used to interconnect Haskell GI and Cairo")
    (license license:lgpl2.1)))

(define-public ghc-haskell-gi-overloading
  (package
    (name "ghc-haskell-gi-overloading")
    (version "1.0")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://hackage.haskell.org/package/haskell-gi-overloading/haskell-gi-overloading-"
             version
             ".tar.gz"))
       (sha256
        (base32 "0ak8f79ia9zlk94zr02sq8bqi5n5pd8ria8w1dj3adcdvpw9gmry"))))
    (build-system haskell-build-system)
    (home-page "https://github.com/haskell-gi/haskell-gi")
    (synopsis "Overloading support for haskell-gi")
    (description
     "Control overloading support in haskell-gi generated bindings")
    (license license:bsd-3)))

(define-public ghc-haskell-gi
  (package
    (name "ghc-haskell-gi")
    (version "0.26.0")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://hackage.haskell.org/package/haskell-gi/haskell-gi-"
             version
             ".tar.gz"))
       (sha256
        (base32 "0k8xpllviq67zm72dwnh5k05x8l42irzh7sdxi1jwjjm26sc3xm9"))))
    (build-system haskell-build-system)
    (inputs
     (list ghc-haskell-gi-base
           ghc-attoparsec
           ghc-pretty-show
           ghc-ansi-terminal
           ghc-safe
           ghc-xdg-basedir
           ghc-xml-conduit
           ghc-regex-tdfa
           gobject-introspection))
    (native-inputs (list ghc-doctest ghc-cabal-doctest pkg-config))
    (home-page "https://github.com/haskell-gi/haskell-gi")
    (synopsis
     "Generate Haskell bindings for GObject Introspection capable libraries")
    (description
     "Generate Haskell bindings for GObject Introspection capable libraries.  This includes most
notably Gtk+, but many other libraries in the GObject ecosystem provide introspection data
too.")
    (license license:lgpl2.1)))

(define-public ghc-haskell-gi-base
  (package
    (name "ghc-haskell-gi-base")
    (version "0.26.0")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://hackage.haskell.org/package/haskell-gi-base/haskell-gi-base-"
             version
             ".tar.gz"))
       (sha256
        (base32 "1skxqs8vp8q42g59j7sa1zr25fnqjahbngpgrc2bqs6r5yj00mb7"))))
    (build-system haskell-build-system)
    (inputs (list gobject-introspection))
    (native-inputs (list pkg-config))
    (home-page "https://github.com/haskell-gi/haskell-gi")
    (synopsis "Foundation for libraries generated by haskell-gi")
    (description "Foundation for libraries generated by haskell-gi")
    (license license:lgpl2.1)))

(define-public gobject-introspection+cairo
  (hidden-package
   (package
     (inherit gobject-introspection)
     (arguments
      (substitute-keyword-arguments (package-arguments gobject-introspection)
        ((#:configure-flags flags ''())
         `(append
           (list
            "-Dcairo=enabled" ; use cairo for tests
            (string-append "-Dcairo_libname="
                           (assoc-ref %build-inputs "cairo")
                           "/lib/libcairo-gobject.so"))
           ,flags))))
     (inputs
      `(("cairo" ,cairo)
        ,@(package-inputs gobject-introspection))))))

(define-public ghc-gi-cairo
  (package
    (name "ghc-gi-cairo")
    (version "1.0.26")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://hackage.haskell.org/package/gi-cairo/gi-cairo-"
             version
             ".tar.gz"))
       (sha256
        (base32 "1f3gfhqg03b64n4ipg3q96wkwsisrs79bbg7335nwg92frkv1j79"))))
    (build-system haskell-build-system)
    (inputs
     (list cairo
           ghc-haskell-gi
           ghc-haskell-gi-base
           ghc-haskell-gi-overloading
           gobject-introspection+cairo))
    (native-inputs (list pkg-config))
    (home-page "https://github.com/haskell-gi/haskell-gi")
    (synopsis "Cairo bindings")
    (description "Bindings for Cairo, autogenerated by haskell-gi.")
    (license license:lgpl2.1)))

(define-public ghc-io-storage
  (package
    (name "ghc-io-storage")
    (version "0.3")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://hackage.haskell.org/package/io-storage/io-storage-"
             version
             ".tar.gz"))
       (sha256
        (base32 "1ga9bd7iri6vlsxnjx765yy3bxc4lbz644wyw88yzvpjgz6ga3cs"))))
    (build-system haskell-build-system)
    (home-page "https://github.com/willdonnelly/io-storage")
    (synopsis "A key-value store in the IO monad.")
    (description
     "This library allows an application to extend the 'global state' hidden inside the IO
monad with semi-arbitrary data. Data is required to be 'Typeable'.  The library provides an
essentially unbounded number of key-value stores indexed by strings, with each key within
the stores also being a string.")
    (license license:bsd-3)))

(define-public ghc-dyre
  (package
    (name "ghc-dyre")
    (version "0.9.1")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://hackage.haskell.org/package/dyre/dyre-"
             version
             ".tar.gz"))
       (sha256
        (base32 "1lrpjvah57p1ld3spcrgqds3claqkd5a9ay8inihl9wws0ifhfmh"))))
    (build-system haskell-build-system)
    (arguments `(#:tests? #f)) ;; TODO fix running tests
    (inputs (list ghc-executable-path ghc-io-storage ghc-xdg-basedir))
    (home-page "https://github.com/willdonnelly/dyre")
    (synopsis "Dynamic reconfiguration in Haskell")
    (description
     "Dyre implements dynamic reconfiguration facilities after the style of Xmonad. Dyre aims
to be as simple as possible without sacrificing features, and places an emphasis on
simplicity of integration with an application.  A full introduction with a complete example
project can be found in the documentation for 'Config.Dyre'")
    (license license:bsd-3)))

(define-public ghc-dbus-hslogger
  (package
    (name "ghc-dbus-hslogger")
    (version "0.1.0.1")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://hackage.haskell.org/package/dbus-hslogger/dbus-hslogger-"
             version
             ".tar.gz"))
       (sha256
        (base32 "0i2y69kagp53cmlb7p3y6ysr9k5wvfd0vcnpwsasyn1jpk6g80zi"))))
    (build-system haskell-build-system)
    (inputs (list ghc-dbus ghc-hslogger ghc-optparse-applicative))
    (home-page "https://github.com/IvanMalison/dbus-hslogger#readme")
    (synopsis "Expose a dbus server to control hslogger")
    (description
     "Please see the README on Github at <https://github.com/IvanMalison/dbus-hslogger#readme>")
    (license license:bsd-3)))

(define-public ghc-broadcast-chan
  (package
    (name "ghc-broadcast-chan")
    (version "0.2.1.1")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://hackage.haskell.org/package/broadcast-chan/broadcast-chan-"
             version
             ".tar.gz"))
       (sha256
        (base32 "01r57v2ghfkv37cwh64zs1lrz1rd7is3pa5k7y8h16ws1ddglirg"))))
    (build-system haskell-build-system)
    (inputs (list ghc-unliftio-core))
    (arguments
     `(#:cabal-revision
       ("2" "0zpbfdgxs3b7wx2qgvrs5y01z8lzvww2b7bmyrg5cj6p5xznllk1")))
    (home-page "https://github.com/merijn/broadcast-chan")
    (synopsis
     "Closable, fair, single-wakeup channel type that avoids 0 reader space leaks.")
    (description
     "__WARNING:__ While the code in this library should be fairly stable and production, the
API is something I'm still working on. API changes will follow the PVP, but __expect__
breaking API changes in future versions! . A closable, fair, single-wakeup channel that
avoids the 0 reader space leak that @\"Control.Concurrent.Chan\"@ from base suffers from.
. The @Chan@ type from @\"Control.Concurrent.Chan\"@ consists of both a read and write end
combined into a single value. This means there is always at least 1 read end for a @Chan@,
which keeps any values written to it alive. This is a problem for applications/libraries
that want to have a channel that can have zero listeners. . Suppose we have an library
that produces events and we want to let users register to receive events. If we use a
channel and write all events to it, we would like to drop and garbage collect any events
that take place when there are 0 listeners. The always present read end of @Chan@ from
base makes this impossible. We end up with a @Chan@ that forever accumulates more and more
events that will never get removed, resulting in a memory leak. . @\"BroadcastChan\"@
splits channels into separate read and write ends. Any message written to a a channel with
no existing read end is immediately dropped so it can be garbage collected. Once a read
end is created, all messages written to the channel will be accessible to that read end. .
Once all read ends for a channel have disappeared and been garbage collected, the channel
will return to dropping messages as soon as they are written. . __Why should I use
\"BroadcastChan\" over \"Control.Concurrent.Chan\"?__ . * @\"BroadcastChan\"@ is closable,
. * @\"BroadcastChan\"@ has no 0 reader space leak, . * @\"BroadcastChan\"@ has comparable
or better performance. . __Why should I use \"BroadcastChan\" over various (closable) STM
channels?__ . * @\"BroadcastChan\"@ is single-wakeup, . * @\"BroadcastChan\"@ is fair, . *
@\"BroadcastChan\"@ performs better under contention.")
    (license license:bsd-3)))

(define-public ghc-hstringtemplate
  (package
    (name "ghc-hstringtemplate")
    (version "0.8.8")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://hackage.haskell.org/package/HStringTemplate/HStringTemplate-"
             version
             ".tar.gz"))
       (sha256
        (base32 "1n8ci0kzjcgnqh4dfpqwlh7mnlzyiqiqc6hc0zr65p0balbg8zbi"))))
    (build-system haskell-build-system)
    (inputs
     (list ghc-blaze-builder ghc-old-locale ghc-semigroups ghc-syb ghc-void))
    (native-inputs
     (list ghc-quickcheck ghc-random ghc-hunit))
    (home-page "http://hackage.haskell.org/package/HStringTemplate")
    (synopsis "StringTemplate implementation in Haskell.")
    (description "A port of the Java library by Terrence Parr.")
    (license license:bsd-3)))

(define-public ghc-configfile
  (package
    (name "ghc-configfile")
    (version "1.1.4")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://hackage.haskell.org/package/ConfigFile/ConfigFile-"
             version
             ".tar.gz"))
       (sha256
        (base32 "057mw146bip9wzs7j4b5xr1x24d8w0kr4i3inri5m57jkwspn25f"))))
    (build-system haskell-build-system)
    (inputs (list ghc-missingh))
    (home-page "http://software.complete.org/configfile")
    (synopsis "Configuration file reading & writing")
    (description
     "Parser and writer for handling sectioned config files in Haskell.  The ConfigFile module
works with configuration files in a standard format that is easy for the user to edit,
easy for the programmer to work with, yet remains powerful and flexible. It is inspired
by, and compatible with, Python's ConfigParser module. It uses files that resemble Windows
.INI-style files, but with numerous improvements.  ConfigFile provides simple calls to both
read and write config files. It's possible to make a config file parsable by this module,
the Unix shell, and make.")
    (license license:bsd-3)))

(define-public ghc-taffybar
  (package
    (name "ghc-taffybar")
    (version "3.3.0")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://hackage.haskell.org/package/taffybar/taffybar-"
             version
             ".tar.gz"))
       (sha256
        (base32 "17ggcv1y3md11sccbb9mpss2qdanlkv7wy098qh28gra9kq4ibgm"))
       (modules '((guix build utils)))
       (snippet
        '(begin
           (substitute* "src/System/Taffybar.hs"
             (("\"-threaded\"")
              "\"-threaded\", \"-dynamic\""))
           #t))))
    (build-system haskell-build-system)
    (inputs
     (list ghc-configfile
           ghc-hstringtemplate
           ghc-x11
           ghc-ansi-terminal
           ghc-broadcast-chan
           ghc-dbus
           ghc-dbus-hslogger
           ghc-dyre
           ghc-either
           ghc-enclosed-exceptions
           ghc-gi-cairo
           ghc-gi-cairo-connector
           ghc-gi-cairo-render
           ghc-gi-gdk
           ghc-gi-gdkpixbuf
           ghc-gi-gdkx11
           ghc-gi-glib
           ghc-gi-gtk
           ghc-gi-gtk-hs
           ghc-gi-pango
           ghc-gtk-sni-tray
           ghc-gtk-strut
           ghc-haskell-gi
           ghc-haskell-gi-base
           ghc-hslogger
           ghc-http-client
           ghc-http-client-tls
           ghc-http-types
           ghc-multimap
           ghc-old-locale
           ghc-rate-limit
           ghc-regex-compat
           ghc-safe
           ghc-scotty
           ghc-split
           ghc-status-notifier-item
           ghc-time-locale-compat
           ghc-time-units
           ghc-transformers-base
           ghc-tuple
           ghc-utf8-string
           ghc-xdg-desktop-entry
           ghc-xdg-basedir
           ghc-xml
           ghc-xml-helpers
           ghc-optparse-applicative
           gtk+
           xmonad))
    (native-inputs (list pkg-config))
    (home-page "https://github.com/taffybar/taffybar")
    (synopsis "A desktop bar similar to xmobar, but with more GUI")
    (description
     "Taffybar is a gtk+3 (through gi-gtk) based desktop information bar, intended primarily
for use with XMonad, though it can also function alongside other EWMH compliant window
managers.  It is similar in spirit to xmobar, but it differs in that it gives up some
simplicity for a reasonable helping of eye candy.")
    (license license:bsd-3)))

(define-public snixembed
  (package
    (name "snixembed")
    (version "0.3.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://git.sr.ht/~steef/snixembed/")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32
         "0yy1i4463q43aq98qk4nvvzpw4i6bid2bywwgf6iq545pr3glfj5"))
       (modules '((guix build utils)))
       (snippet
        '(begin
           (substitute* "makefile"
             (("valac")
              "valac --cc=gcc"))
           #t))))
    (build-system gnu-build-system)
    (inputs (list gdk-pixbuf gtk+ libdbusmenu))
    (native-inputs (list pkg-config vala))
    (arguments
     `(#:tests? #f ; there are no tests
       #:make-flags (list (string-append "PREFIX=" %output))
       #:phases
       (modify-phases %standard-phases
         (delete 'configure))))
    (synopsis "Proxy StatusNotifierItems as XEmbedded systemtray-spec icons")
    (home-page "https://git.sr.ht/~steef/snixembed")
    (description "Proxy StatusNotifierItems as XEmbedded systemtray-spec icons")
    (license license:isc)))

;ghc-taffybar
;ghc-status-notifier-item
;snixembed

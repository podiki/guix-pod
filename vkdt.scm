(define-module (vkdt)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages image)
  #:use-module (gnu packages llvm)
  #:use-module (gnu packages photo)
  #:use-module (gnu packages rsync)
  #:use-module (gnu packages vulkan)
  #:use-module (gnu packages xiph)
  #:use-module (gnu packages xml)
  #:use-module (guix build-system gnu)
  #:use-module (guix gexp)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module ((guix licenses) #:prefix license:))

(define-public ansel
  (let ((commit "eb70788d22da21caa6af0d859cecd22b8bc6f095")
        (revision "0"))
    (package
      (inherit darktable)
      (name "ansel")
      (version (git-version "0.0.0" "0" commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/aurelienpierreeng/ansel")
               (commit commit)
               ;; rawspeed etc
               (recursive? #t)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "1g4xxafcchaj9zi8x0aik7sq0dz0jy8jlcxzd7y91dkw1z24kq6a"))))
      (arguments
       (substitute-keyword-arguments (package-arguments darktable)
         ;; Don't build or run the tests, due to a build failure on
         ;; current commit. See upstream report
         ;; <https://github.com/aurelienpierreeng/ansel/issues/208>.
         ((#:configure-flags flags #~'())
          #~(delete "-DBUILD_TESTING=On" #$flags))
         ((#:tests? tests #~'())
          #f)
         ((#:phases phases '%standard-phases)
          ;; Rewrite the custom phases from darktable due to the
          ;; package name change.
          #~(modify-phases #$phases
              (delete 'set-LDFLAGS)
              (delete 'wrap-program)
              (add-before 'configure 'set-LDFLAGS
                (lambda _
                  (setenv "LDFLAGS"
                          (string-append "-Wl,-rpath=" #$output "/lib/ansel"))))
              (add-after 'install 'wrap-program
                (lambda _
                  (wrap-program (string-append #$output "/bin/ansel")
                    ;; For GtkFileChooserDialog.
                    `("GSETTINGS_SCHEMA_DIR" =
                      (,(string-append #$(this-package-input "gtk+")
                                       "/share/glib-2.0/schemas"))))))))))
      (synopsis "A darktable fork minus the bloat plus some design vision ")
      (description
       "Ansel is a better future for Darktable, designed from real-life use
cases and solving actual problems, by the guy who did the
scene-referred workflow and spent these past 4 years working full-time
on Darktable.

It is forked on Darktable 4.0, and is compatible with editing
histories produced with Darktable 4.0 and earlier. It is not
compatible with Darktable 4.2 and later and will not be, since 4.2
introduces irresponsible choices that will be the burden of those who
commited them to maintain, and 4.4 will be even worse.")
      (home-page "https://ansel.photos"))))

(define-public vkdt
  (package
    (name "vkdt")
    (version "0.6.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/hanatos/vkdt")
             (commit version)
             ;; rawspeed and (custom) imgui bundled
             (recursive? #t)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "110pvvxgp281g5pnrfkjqyqwxmk1djraxnimf6jzpqrm2iy3vbdv"))))
    (build-system gnu-build-system)
    (arguments
     (list
      #:tests? #f ; no tests
      #:make-flags
      #~(list
         (string-append "DESTDIR=" #$output)
         ;; The Makefile uses a lowercase "prefix"
         (string-append "prefix="))
      #:phases
      #~(modify-phases %standard-phases
          ;; Guix's vulkan doesn't have pkg-config files.
          (add-after 'unpack 'lvulkan
            (lambda _
              (substitute* "src/qvk/flat.mk"
                (("\\$\\(shell pkg-config --libs vulkan\\)") "-lvulkan"))))
          ;; no configure
          (delete 'configure)
          ;; The Makefile for building is in a different directory.
          (add-before 'build 'build-chdir
            (lambda* _
              (chdir "bin")))
          (add-before 'install 'install-chdir
            (lambda* _
              (chdir ".."))))))
    (inputs (list glfw
                  glslang
                  libjpeg-turbo
                  libomp
                  libvorbis
                  libxml2
                  pugixml
                  rsync ; for install
                  vulkan-headers
                  vulkan-loader
                  zlib))
    (native-inputs (list clang cmake pkg-config))
    (synopsis "Darktable which sucks less")
    (description
     "this is an experimental complete rewrite of darktable, at this point with a reduced
feature set. vkdt is designed with high performance in mind. there are already some new
features, too: support for animations, raw video, and heavy lifting algorithms like image
alignment and better highlight inpainting. this is made possible by faster processing,
allowing more complex operations.

the processing pipeline has been rewritten as a generic node graph (DAG) which supports
multiple inputs and multiple outputs. all processing is done in glsl shaders/vulkan. this
facilitates potentially heavy duty computational photography tasks, for instance aligning
multiple raw files and merging them before further processing, as well as outputting
intermediate results for debugging. the gui profits from this scheme as well and can
display textures while they are still on GPU and output the data to multiple targets, such
as the main view and histograms.")
    (home-page "https://jo.dreggn.org/vkdt/")
    (license license:bsd-2)))

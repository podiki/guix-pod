;;; GNU Guix --- Functional package management for GNU
;;; Copyright © 2022 John Kehayias <john.kehayias@protonmail.com>
;;;
;;; This file is part of GNU Guix.
;;;
;;; GNU Guix is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; GNU Guix is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with GNU Guix.  If not, see <http://www.gnu.org/licenses/>.

(define-module (peroxide-service)
  #:use-module (peroxide)
  #:use-module (gnu packages admin)
  #:use-module (gnu services)
  #:use-module (gnu services shepherd)
  #:use-module (gnu system accounts)
  #:use-module (gnu system shadow)
  #:use-module (guix gexp)
  #:use-module (guix records)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:export (peroxide-configuration
            peroxide-configuration?
            peroxide-service-type))

;;; Commentary:
;;;
;;; This module provides a service definition for the peroxide service.
;;;
;;; Code:

(define-record-type* <peroxide-configuration>
  peroxide-configuration make-peroxide-configuration
  peroxide-configuration?
  (user              peroxide-configuration-user
                     (default "peroxide"))
  (imap-port         peroxide-configuration-imap-port
                     (default "1143"))
  (smtp-port         peroxide-configuration-smtp-port
                     (default "1025"))
  (allow-proxy       peroxide-configuration-allow-proxy
                     (default "false"))
  (cache-enabled     peroxide-configuration-cache-enabled
                     (default "true"))
  (cache-compression peroxide-configuration-cache-compression
                     (default "true"))
  (cache-dir         peroxide-configuration-cache-dir
                     (default "/var/cache/peroxide"))
  (x509-key          peroxide-configuration-x509-key
                     (default "/etc/peroxide/key.pem"))
  (x509-cert         peroxide-configuration-x509-cert
                     (default "/etc/peroxide/cert.pem"))
  (cookie-jar        peroxide-configuration-cookie-jar
                     (default "/etc/peroxide/cookies.json"))
  (credentials-store peroxide-configuration-credentials-store
                     (default "/etc/peroxide/credentials.json"))
  (server-address    peroxide-configuration-server-address
                     (default "[::0]"))
  (bcc-self          peroxide-configuration-bcc-self
                     (default "false")))

(define (peroxide-configuration->file config)
  (apply
   mixed-text-file "peroxide.conf"
   (append '("{\n")
           (map (match-lambda
                  ((config-name config-val)
                   (string-append config-name ": \"" (config-val config) "\",\n")))
                `(("UserPortImap"     ,peroxide-configuration-imap-port)
                  ("UserPortSmtp"     ,peroxide-configuration-smtp-port)
                  ("AllowProxy"       ,peroxide-configuration-allow-proxy)
                  ("CacheEnabled"     ,peroxide-configuration-cache-enabled)
                  ("CacheCompression" ,peroxide-configuration-cache-compression)
                  ("CacheDir"         ,peroxide-configuration-cache-dir)
                  ("X509Key"          ,peroxide-configuration-x509-key)
                  ("X509Cert"         ,peroxide-configuration-x509-cert)
                  ("CookieJar"        ,peroxide-configuration-cookie-jar)
                  ("CredentialsStore" ,peroxide-configuration-credentials-store)
                  ("ServerAddress"    ,peroxide-configuration-server-address)
                  ("BCCSelf"          ,peroxide-configuration-bcc-self)))
           '("\n}"))))

(define (peroxide-shepherd-service config)
  (shepherd-service
   (documentation "Run peroxide (a ProtonMail bridge)")
   (requirement '(user-processes networking))
   (provision '(peroxide))
   (start #~(make-forkexec-constructor
             (list (string-append #$peroxide "/sbin/peroxide")
                   "-config"
                   ;; This has been copied to /etc/peroxide.conf but we use
                   ;; the store version.
                   #$(peroxide-configuration->file config)
                   ;; fix:
                   "-log-file=/var/log/peroxide/peroxide.log"
                   "-log-level" "Info")
             #:user #$(peroxide-configuration-user config)
             ;; todo?
             #:group #$(peroxide-configuration-user config)
             ;#:log-file #$(mpd-file-name config "log")
             ))
   (stop  #~(make-kill-destructor))))

(define (peroxide-service-activation config)
  (with-imported-modules '((guix build utils))
    #~(begin
        (use-modules (guix build utils))
        (define %user
          (getpw #$(peroxide-configuration-user config)))
        (define* (create-self-signed-certificate-if-absent
                  #:key (key-file #$(peroxide-configuration-x509-key config))
                  (cert-file #$(peroxide-configuration-x509-cert config))
                  (owner %user)
                  ;; didn't work (cert with no common name) when using guix system init and then booting, had no hostname?
                  ;; works on system reconfigure
                  (common-name (gethostname))
                  (organization-name "Guix"))
          (unless (or (file-exists? key-file)
                      (file-exists? cert-file))
            (cond
             ((zero? (system* (string-append #$peroxide "/sbin/peroxide-cfg")
                              "-config" #$(peroxide-configuration->file config)
                              "-action" "gen-x509" "-x509-key" key-file "-x509-cert" cert-file
                              "-x509-org" organization-name
                              "-x509-cn" common-name))
              (chown key-file (passwd:uid owner) (passwd:gid owner))
              (chmod key-file #o400)
              (chown cert-file (passwd:uid owner) (passwd:gid owner))
              (chmod cert-file #o400))
             (else
              (format (current-error-port)
                      "Failed to create keys at ~a and ~a.\n" key-file cert-file)))))
        (let ((cache-dir #$(peroxide-configuration-cache-dir config)))
          (mkdir-p/perms "/etc/peroxide" %user #o700)
          (mkdir-p/perms cache-dir %user #o700)
          ;; this didn't work? wasn't created (when moving to new hd and after system init and restart)
          ;; works on system reconfigure
          (mkdir-p/perms "/var/log/peroxide" %user #o750)
          ;; Called with the defaults specified to be explicit.
          (create-self-signed-certificate-if-absent
           #:key-file #$(peroxide-configuration-x509-key config)
           #:cert-file #$(peroxide-configuration-x509-cert config)
           #:owner %user
           #:common-name (gethostname)
           #:organization-name "Guix")
          ;; Copy the configuration file for when the user runs e.g. peroxide-cfg.
          (copy-file #$(peroxide-configuration->file config) "/etc/peroxide.conf")
          (chown "/etc/peroxide.conf" (passwd:uid %user) (passwd:gid %user))
          (chmod "/etc/peroxide.conf" #o400)
          ;; peroxide-cfg is run as root, so chown the cache created
          (for-each (lambda (file)
                      (chown file (passwd:uid %user) (passwd:gid %user)))
                    (find-files cache-dir #:directories? #t))
          ;; logrotate?
          ))))

(define %peroxide-account
  ;; Default account and group for peroxide.
  (list (user-group (name "peroxide") (system? #t))
        (user-account
         (name "peroxide")
         (group "peroxide")
         (system? #t)
         (comment "peroxide service user")
         (home-directory "/var/empty")
         (shell (file-append shadow "/sbin/nologin")))))

(define peroxide-service-type
  (service-type
   (name 'peroxide)
   (description "Run peroxide (a ProtonMail bridge)")
   (extensions
    (list (service-extension shepherd-root-service-type
                             (compose list peroxide-shepherd-service))
          (service-extension account-service-type
                             (const %peroxide-account))
          (service-extension activation-service-type
                             peroxide-service-activation)))
   (default-value (peroxide-configuration))))
